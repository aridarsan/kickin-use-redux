import axios from 'axios';

// const baseUrl = 'http://kickin.southeastasia.cloudapp.azure.com';
const baseUrl = 'http://13.76.129.177';

// AUTH
export function apiLogin(dataLogin) {
  return axios({
    method: 'POST',
    url: baseUrl + '/auth/login',
    data: dataLogin,
  });
}

export function apiLoginGoogle(dataLogin) {
  return axios({
    method: 'POST',
    url: baseUrl + '/auth/login/google',
    data: dataLogin,
  });
}

export function apiRegister(dataRegister) {
  return axios({
    method: 'POST',
    url: baseUrl + '/auth/register',
    data: dataRegister,
  });
}

export function apiRegisterGoogle(dataRegister) {
  return axios({
    method: 'POST',
    url: baseUrl + '/auth/register/google',
    data: dataRegister,
  });
}
// PROFILE

export function apiGetProfile(id, headers) {
  return axios({
    method: 'GET',
    url: baseUrl + '/profile?id=' + id,
    headers,
  });
}

export function apiEditProfile(id, headers, dataEditProfile) {
  return axios({
    method: 'PATCH',
    url: baseUrl + '/profile?id=' + id,
    headers,
    data: dataEditProfile,
  });
}

// FIELD
export function apiGetAllField() {
  return axios({
    method: 'GET',
    url: baseUrl + '/fields/read/all',
  });
}

export function apiGetAllFieldDetail(id, headers) {
  return axios({
    method: 'GET',
    url: baseUrl + '/fields/info?id=' + id,
    headers,
  });
}

// REVIEW
export function apiGetReview(id, headers) {
  return axios({
    method: 'GET',
    url: baseUrl + '/review/read/' + id,
    headers,
  });
}

export function apiGetUserReview(id, headers) {
  return axios({
    method: 'GET',
    url: baseUrl + '/review/read/' + id,
    headers,
  });
}

export function apiEditReview(id, headers, dataEditReview) {
  return axios({
    method: 'PATCH',
    url: baseUrl + '/review/edit?id=' + id,
    data: dataEditReview,
    headers,
  });
}

// BOOKING
export function apiGetBookingFieldList(payload, headers) {
  return axios({
    method: 'GET',
    url: baseUrl + '/booking?id=' + payload,
    headers,
  });
}
export function apiBooking(id, headers, dataBooking) {
  return axios({
    method: 'POST',
    url: baseUrl + '/booking/' + id,
    headers,
    data: dataBooking,
  });
}

export function apiGetBookingUser(id, headers) {
  return axios({
    method: 'GET',
    url: baseUrl + '/booking/user/list?id=' + id,
    headers,
  });
}

// TICKETING

export function apiAddTicket(id, headers) {
  return axios({
    method: 'POST',
    url: baseUrl + '/ticket?id=' + id,
    headers,
  });
}
export function apiGetTicket(id, headers) {
  return axios({
    method: 'GET',
    url: baseUrl + '/ticket/read?q=' + id,
    headers,
  });
}

// RENTAL KIT

export function apiGetRentalItems(headers) {
  return axios({
    method: 'GET',
    url: baseUrl + '/rent/items',
    headers,
  });
}

export function apiPostSocks(id, headers, payload) {
  delete payload.id;
  return axios({
    method: 'POST',
    url:
      baseUrl +
      '/user/rent/list/' +
      id +
      '?id=28842a19-b57b-4bcc-ba8c-6ccd64406c34',
    data: payload,
    headers,
  });
}

export function apiPostShoes(id, headers, payload) {
  delete payload.id;
  return axios({
    method: 'POST',
    url:
      baseUrl +
      '/user/rent/list/' +
      id +
      '?id=47302d5a-12c3-4f32-8cd5-56e5b95daf50',
    data: payload,
    headers,
  });
}

export function apiPostVest(id, headers, payload) {
  delete payload.id;
  return axios({
    method: 'POST',
    url:
      baseUrl +
      '/user/rent/list/' +
      id +
      '?id=1e31d25a-d9f0-4905-bbcd-df75873c5965',
    data: payload,
    headers,
  });
}

// TRANSACTION

export function apiPostTRX(id, headers) {
  return axios({
    method: 'POST',
    url: baseUrl + '/transaction/post/' + id,
    headers,
  });
}

export function apiGetTRX(id, headers) {
  return axios({
    method: 'GET',
    url: baseUrl + '/transaction/read/' + id,
    headers,
  });
}

// PAYMENT

export function apiPayment(id, headers, payload) {
  return axios({
    method: 'POST',
    url: baseUrl + '/payment/' + id,
    data:payload,
    headers,
  });
}

// ADMIN

export function apiLoginAdmin(data) {
  return axios({
    method: 'POST',
    url: baseUrl + '/auth/login/admin',
    data: data,
  });
}

export function apiGetPaymentAdmin(headers) {
  return axios({
    method: 'GET',
    url: baseUrl + '/payment/admin/list',
    headers,
  });
}

export function apiGetBookingAdmin(headers) {
  return axios({
    method: 'GET',
    url: baseUrl + '/booking/admin/list',
    headers,
  });
}

export function apiPatchPayment(id, headers, data) {
  return axios({
    method: 'PATCH',
    url: baseUrl + '/payment/admin/list?id=' + id,
    data: data,
    headers,
  });
}

export function apiPatchBooking(id, headers, data) {
  return axios({
    method: 'PATCH',
    url: baseUrl + '/booking/admin/list?id=' + id,
    data:data,
    headers,
  });
}

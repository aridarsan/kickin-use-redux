import { Button, Card, CardBody, Col, Container, Row } from "reactstrap";
import "../styles/FieldDetail.css";
import "@fortawesome/fontawesome-free";
import { Rating } from "@material-ui/lab";
import { useParams } from "react-router-dom";
import { useEffect } from "react";
import { useState } from "react";
import axios from "axios";
import swal from "sweetalert";
import Loading from "./Loading";
import ModalBookingField from "./ModalBookingField";

const FieldDetail = (props) => {
  const url = "https://binar8-novien-ghoziana.nandaworks.com"
  // const url = "http://kickin.southeastasia.cloudapp.azure.com";

  const params = useParams();
  // const history = useHistory();
  const [fieldDetail, setFieldDetail] = useState([]);
  const [loading, setLoading] = useState(false);

  const urlPhoto = `${url}/fields/info?id=${params.id}`;

  useEffect(() => {
    axios.get(urlPhoto).then((res) => {
      setFieldDetail(res.data);
      // console.log(res.data);
      setLoading(true);
    })
    .catch((err)=>{
      // console.log(err)
      swal({
        icon: "warning",
        title: "Failed to get data",
        text: "Please wait",
        type: "warning",
        buttons: false,
        timer: 3000,
      });
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // console.log(props.isLogin);

  return (
    <Container>
      {fieldDetail && loading ? (
        <>
          <Row className="detail-top">
            <>
              <div className="col-12 col-sm-4 col-md-8 field-img">
                {fieldDetail.fieldPhotos.slice(0, 1).map((photoBig) => (
                  <div key={photoBig.photoUrl}>
                    <img
                      src={photoBig.photoUrl}
                      alt="field1"
                      className="col-sm-4 col-md-12 field-img-big"
                    />
                  </div>
                ))}

                <div className="position-relative overflow-hidden">
                  {fieldDetail.fieldPhotos.slice(1, 5).map((photoSmall) => (
                    <img
                      src={photoSmall.photoUrl}
                      alt="field small"
                      md={6} xs={12}
                      className="col-3 img-sm"
                      // onClick={() => photoBig(fieldDetail.id)}
                    />
                  ))}
                </div>
              </div>
            </>

            <Col className="detail-text">
              <div className="desc-field">
                <h2 className="field-name">{fieldDetail.name}</h2>
                <p className="field-loc">
                  <i class="fas fa-map-marker-alt"></i>
                  {fieldDetail.address}
                </p>
                <h4 className="description-title">Description</h4>
                <p className="description-p">{fieldDetail.description}</p>
              </div>
              <div className="action-book">
                <h3 className="price-field">
                  Rp. {fieldDetail.price} /hour <i class="fas fa-tag ml-2"></i>
                </h3>
                <a href={fieldDetail.addressUrl} target="blank_">
                  <Button className="col-12 mb-3 btn-direction">
                    <i class="fas fa-location-arrow mr-2"></i>
                    Direction
                  </Button>
                </a>

                {/* <Link className="col-5 view ml-auto">
                  <Button
                    block
                    className="btn-primary"
                    onClick={() => {
                      history.push(`/booking-page/${fieldDetail.id}`);
                    }}
                  >
                    <i class="fas fa-bookmark mr-2"></i> Book
                  </Button>
                </Link> */}

                <ModalBookingField
                  name={fieldDetail.name}
                  id={fieldDetail.id}
                />
              </div>
            </Col>
          </Row>

          <Row className="mt-3 mb-3">
            <Col>
              <h3>Feedback and Review</h3>
            </Col>
            <Rating
              name="half-rating-read"
              defaultValue="1"
              precision={1}
              max={1}
              readOnly
            />
            <h3 className="ml-auto mr-2">
              {fieldDetail.average} /{" "}
              <span style={{ color: "#545454" }}>5</span>
            </h3>
          </Row>

          <Row className="review-user">
            <Col sm="8">
              {fieldDetail.reviews === null ? (
                <h3>No Review yet</h3>
              ) : (
                fieldDetail.reviews.map((review) => (
                  <Card
                    className="col-sm-12 mb-3"
                    style={{
                      backgroundColor: "#313131",
                      border: "none",
                    }}
                  >
                    <CardBody>
                      <h4 className="review-title">
                        <b>{review.user.fullname}</b>
                      </h4>
                      <Rating
                        name="half-rating-read"
                        defaultValue={review.rating}
                        precision={0.2}
                        max={5}
                        readOnly
                      />
                      {/* <Rating name="half-rating-read" defaultValue={data.rating} precision={0.5} max={10} readOnly /><p><b>{data.rating} / 10</b></p> */}

                      <p className="review-p">{review.comment}</p>
                    </CardBody>
                  </Card>
                ))
              )}
            </Col>

            <Col sm="4" className="instruction">
              <br />
              <h4>INSTRUCTION FOR BOKING FIELD</h4>
              <ol>
                <li>Choose field what you want to booking</li>
                <li>Select date and time for booking</li>
                <li>Submit the booking field</li>
                <li>
                  Choose the rental kit if you want and direct to make
                  transaction if you won't to rental
                </li>
                <li>
                  Please upload confirmation payment in 24 hours in profile
                  page, or it's automatically canceled
                </li>
                <li>
                  Admin will view your receipt, and accept or decline your
                  booking
                </li>
                <li>
                  you can see the ticket if admin has accepted your booking, and
                  enjoy to play futsal
                </li>
              </ol>
            </Col>
          </Row>
        </>
      ) : (
        <Loading />
      )}
    </Container>
  );
};

export default FieldDetail;

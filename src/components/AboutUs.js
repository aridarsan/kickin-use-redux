import React from "react";
import { Row, Col, Container } from "reactstrap";
import "../styles/AboutUs.css";

const AboutUs = () => {
  return (
    <div>
      <div>
        <Container>
          <h1 className="kickin">KICKIN?</h1>
          <div className="AboutUs-background">
            <h2 className="ourbackground">Our Background</h2>
            <Row>
              <Col
                style={{
                  border: "1px solid #FFCB74",
                  padding: "10px",
                  borderRadius: "5px",
                  margin: "5px",
                }}
              >
                <Col>
                  <h3 className="time">Time</h3>
                  <hr style={{ border: "1px solid #FFCB74" }} />
                  <text className="par">
                    Nowadays, people need easy access to find suitable prices
                    and location to play futsal with friends in the middle of
                    their busy time.
                  </text>
                </Col>
              </Col>
              <Col
                style={{
                  border: "1px solid #FFCB74",
                  padding: "10px",
                  borderRadius: "5px",
                  margin: "5px",
                }}
              >
                <Col>
                  <h3 className="information">Information</h3>
                  <hr style={{ border: "1px solid #FFCB74" }} />
                  <text className="par">
                    People need real information such as location, qualities,
                    and field price's to make sure they can play futsal
                    comfortably at on affordable price
                  </text>
                </Col>
              </Col>
              <Col
                style={{
                  border: "1px solid #FFCB74",
                  padding: "10px",
                  borderRadius: "5px",
                  margin: "5px",
                }}
              >
                <Col>
                  <h3 className="behaviour">Behaviour</h3>
                  <hr style={{ border: "1px solid #FFCB74" }} />
                  <text className="par">
                    In this day and age, people can access all of the sources of
                    information from their smartphones. Unfortunately, only a
                    few applications can help everybody discover a suitable
                    place for playing futsal with their friends and that's even
                    rarely seen
                  </text>
                </Col>
              </Col>
            </Row>
          </div>
          <div>
            <Row className="mt-3">
              <Col
                style={{
                  border: "1px solid #FFCB74",
                  padding: "10px",
                  borderRadius: "5px",
                  margin: "5px",
                }}
              >
                <Col>
                  <div className="AboutUs-goals">
                    <h2 className="goals">Goals</h2>
                    <hr style={{ border: "1px solid #FFCB74" }} />
                    <text className="par">
                      A one-stop platform for futsal lovers to make easy access
                      to find suitable place and price for playing futsal with
                      the best of user interface and experience.
                    </text>
                    \
                    <br />
                    <text className="par">
                      We believe there is an opportunity to create a great
                      platform that brings people together with one eye on the
                      future and think likely what that be might look like.
                    </text>
                  </div>
                </Col>
              </Col>
              <Col
                style={{
                  border: "1px solid #FFCB74",
                  padding: "10px",
                  borderRadius: "5px",
                  margin: "5px",
                }}
              >
                <Col>
                  <div className="AboutUs-features">
                    <h2 className="goals">Features</h2>
                    <hr style={{ border: "1px solid #FFCB74" }} />
                    <ul className="ul">
                      <li className="li-features">
                        <text className="par">
                        ✓ Easy and effective booking process
                        </text>
                      </li>
                      <li className="li-features">
                        <text className="par">
                        ✓ Very User friendly interfaces
                        </text>
                      </li>
                      <li className="li-features">
                        <text className="par">
                        ✓ Trusted and verified field informations
                        </text>
                      </li>
                    </ul>
                  </div>
                  howdoweuseit
                </Col>
              </Col>
            </Row>
          </div>
        </Container>
        <Container>
          <Row className="mt-3">
            <Col
              style={{
                border: "1px solid #FFCB74",
                padding: "10px",
                borderRadius: "5px",
                margin: "5px",
              }}
            >
              <Col className="use-it">
                <h2 className="howdoweuseit">How do we use it?</h2>
                <hr style={{ border: "1px solid #FFCB74" }} />
                <ol>
                  <li className="li">
                    <text className="steps">
                      Register to create a new account
                    </text>
                  </li>
                  <li className="li">
                    <text className="steps">
                      Log in with your recently made account
                    </text>
                  </li>
                  <li className="li">
                    <text className="steps">
                      To start searching preferred field, click the browse menu
                      on the top left navbar screen.
                    </text>
                  </li>
                  <li className="li">
                    <text className="steps">
                      Once you're on the browse field page, search your
                      preferred fields, you can ease your field browsing by
                      sorting them and search it's exact name and location.
                    </text>
                  </li>
                  <li className="li">
                    <text className="steps">
                      Once you found your preferred field, click view to see
                      information of the field.
                    </text>
                  </li>
                  <li className="li">
                    <text className="steps">
                      Start booking by clicking the book button, select choice
                      of date and duration, once you're set, go to your profile
                      page menu which is located on the top right.
                    </text>
                  </li>
                  <li className="li">
                    <text className="steps">
                      On the right side of the screen, you'll be shown an entry
                      which is showing your booking status, you can start the
                      payment process by clicking the upload receipt button,
                      which contains total payment and which bank account should
                      you transfer the payment.
                    </text>
                  </li>
                  <li className="li">
                    <text className="steps">
                      Upload your photo-taken receipt, then wait until admin
                      verifies your transaction.
                    </text>
                  </li>
                  <li className="li">
                    <text className="steps">
                      Once admin verified the transaction process, you'll be
                      provided a ticket, Congratulations!!! And please make sure
                      that you've screencapped the ticket for the entrance of
                      your preferred field.
                    </text>
                  </li>
                </ol>
              </Col>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
};

export default AboutUs;

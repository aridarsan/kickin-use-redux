import React, { useRef, useState } from "react";
import { Row, Col, Table, Button, Modal, Spinner, ModalBody } from "reactstrap";
import { useHistory } from "react-router-dom";
import receipt from "../icon/upload.png";
import swal from "sweetalert";
import axios from "axios";
import Uploady from "@rpldy/uploady";
import "../styles/PaymentConfirmation.css";

const PaymentConfirmation = (props) => {
  const inputFile = useRef(null);
  const onTextClick = () => {
    inputFile.current.click();
  };
  const [modalPayment, setModalPayment] = useState(false);
  const togglePayment = () => setModalPayment(!modalPayment);

  const [rawImage, setRawImage] = useState(null);
  const [isSubmitting, setIsSubmitting] = useState(false);

  //url
  const baseUrl = "https://binar8-novien-ghoziana.nandaworks.com";

  //save data needed
  const token = localStorage.getItem("token");

  const history = useHistory();

  //authorization
  var config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + token,
    },
  };

  // const getTransactionData = () => {
  //   axios
  //     .get(`${baseUrl}/transaction/read/${props.id}`, config)
  //     .then((res) => {
  //       console.log("berhasil get data", res.data.data);
  //       setDataPayment(res.data.data);
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });
  // };

  // console.log(props.transactionId);
  // console.log(props.transactionId.id);
  // console.log(props.transactionId.totalPrice);

  const submitPayment = () => {
    // e.preventefault();
    setIsSubmitting(true);
    const formdata = new FormData();
    formdata.append("file", rawImage);
    // console.log(rawImage);

    axios
      .post(`${baseUrl}/payment/${props.transactionId.id}`, formdata, config)
      .then((res) => {
        // console.log(res);
        handlePostTicket();
        togglePayment(false);
        history.push("/profile");
        togglePayment();
        setIsSubmitting(false);
        swal({
          icon: "success",
          title: `Your receipt has been upload`,
          text: "Please wait for Confirmation",
          type: "success",
          buttons: false,
          timer: 3000,
        });
      })
      .catch((err) => {
        // console.log(err);
        swal({
          icon: "warning",
          title: "Failed to upload receipt",
          text: "Please try again",
          type: "warning",
          buttons: false,
          timer: 3000,
        });
      });
    setIsSubmitting(false);
  };
  
// console.log(props.id)
  const handleCancel = (e) => {
    e.preventDefault();
    setIsSubmitting(true);
    const data = {
      status: "cancelled",
    };
    // console.log(data);
    axios
      .patch(`${baseUrl}/booking/${props.id}`, data, config)
      .then((res) => {
        // console.log(res);
        togglePayment();
        swal({
          icon: "success",
          title: `Your Booking has been canceled`,
          text: "make sure you doing the right thing",
          type: "success",
          buttons: false,
          timer: 3000,
        });
        setIsSubmitting(false);
      })
      .catch((err) => {
        // console.log(err);
        swal({
          icon: "warning",
          title: "Failed to cancel booking",
          text: "Please try again",
          type: "warning",
          buttons: false,
          timer: 3000,
        });
        setIsSubmitting(false);
      });
  };
    
    const handlePostTicket = () => {
    const data = {};
    axios
      .post(`${baseUrl}/ticket/${props.id}`,data, config)
      .then((res) => {
        // console.log(res);
      })
      .catch((err) => console.log(err));
  };

  return (
    <>
      <Button
        color="info"
        onClick={togglePayment}
        className="direction mt-3 mb-3"
      >
        <i class="fas fa-receipt mr-2"></i>
        <strong>UPLOAD RECEIPT</strong>
      </Button>
      <Modal
        isOpen={modalPayment}
        toggle={togglePayment}
        className="custom-modal-size"
      >
        {props.transactionId !== undefined ? (
          <>
            <div className="modal-content">
              <div className="modal-body">
                <Row>
                  <Col>
                    <Button onClick={togglePayment} close aria-label="Cancel">
                      <span aria-hidden>
                        <i class="fas fa-times-circle"></i>
                      </span>
                    </Button>
                  </Col>
                </Row>
                <h3 className="modal-title">Payment Confirmation</h3>
                <br />
                <h4>TRANSFER TO</h4>
                <Table className="table-borderless">
                  <tr>
                    <th scope="row">Name</th>
                    <td>PT. KICKIN MAJU</td>
                  </tr>
                  <tr>
                    <th scope="row">No. Account</th>
                    <td>5255338878 (Bank BCA)</td>
                  </tr>
                  <tr>
                    <th scope="row" className="total">
                      Field Name
                    </th>
                    <td className="total">{props.name}</td>
                  </tr>
                  <tr>
                    <th scope="row" className="total">
                      Total amount
                    </th>
                    <td className="total">
                      Rp. {props.transactionId.totalPrice}
                    </td>
                  </tr>
                </Table>
                <h4>UPLOAD RECEIPT</h4>
                <br />
                <form method="post">
                  <Row>
                    <Uploady
                      multiple
                      grouped
                      maxGroupSize={2}
                      method="POST"
                      destination={{
                        url: `${baseUrl}/payment/${props.transactionId.id}`,
                        headers: {
                          "content-type": "multipart/form-data",
                          Authorization: "Bearer " + token,
                        },
                      }}
                    >
                      <Col sm="6">
                        <img
                          src={rawImage !== null ? rawImage : receipt}
                          alt=""
                          width="100%"
                        />
                      </Col>
                      <Col style={{ textAlign: "center" }}>
                        <input
                          onChange={(e) => setRawImage(e.target.files[0])}
                          type="file"
                          id="myfile"
                          ref={inputFile}
                        />
                        <Button
                          block
                          onClick={onTextClick}
                          style={{ textAlign: "center" }}
                        >
                          Select File
                        </Button>
                      </Col>
                    </Uploady>
                  </Row>
                  <br />
                  <p>
                    After upload your receipt and click submit, admin check the
                    receipt if our admin confirm your receipt. You wil get your
                    ticket on Profile
                  </p>
                  <Button
                    block
                    className="btn btn-primary"
                    value="Submit"
                    onClick={submitPayment}
                  >
                    {isSubmitting === true ? <Spinner color="light" /> : "Submit"}
                  </Button>
                  <br />
                  <Button
                    block
                    className="btn-second"
                    color="danger"
                    value="Submit"
                    onClick={handleCancel}
                  >
                    {isSubmitting === true ? <Spinner color="light" /> : "Cancel Booking"}
                  </Button>
                </form>
              </div>
            </div>
          </>
        ) : (
          <div>
            <ModalBody
              style={{
                width: "400px",
                height: "300px",
                margin: "50px",
                padding: "0px",
              }}
            >
              <Row>
                <Col>
                  <Button onClick={togglePayment} close aria-label="Cancel">
                    <span aria-hidden>
                      <i class="fas fa-times-circle"></i>
                    </span>
                  </Button>
                </Col>
              </Row>

              <Col>
                <h4 style={{ textAlign: "center", color: "#53c9c2" }}>
                  Hello User <br />
                  you have'nt transaction for confirm. please submit transaction
                  first, on book - rental or submit transaction button
                </h4>
                <h5 style={{ textAlign: "center" }}>
                  if you want confirm the payment
                </h5>
              </Col>
            </ModalBody>
          </div>
        )}
      </Modal>
    </>
  );
};

export default PaymentConfirmation;

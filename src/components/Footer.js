import React from "react";
import "../styles/Footer.css";
import { Container, Row, Col } from "reactstrap";
import { Link } from "react-router-dom";
import googlePlay from "../icon/google-play.png";
import appStore from "../icon/apple-store.png";
import facebook from "../icon/facebook.svg";
import instagram from "../icon/instagram.svg";
import pinterest from "../icon/pinterest.svg";
import youtube from "../icon/youtube.svg";

const Footer = () => {
  return (
    <div className="footer">
      <Container className="text-white">
        <hr style={{ border: "1px solid #545454" }} />
        <hr style={{ height: "5px", border: "2px solid #545454" }} />
        <Row className="content-row">
          <Col lg="6">
            <Link to="/" className="brand d-flex align-items-center">
              <h2 className="mt-2 logo" style={{ textAlign: "center" }}>
                <em>KICKIN</em>
              </h2>
            </Link>
            <p className="desc-brand">
              A one-stop platform for futsal lovers to make easy access to find
              suitable place and price for playing futsal with the best of user
              interface and experience. We believe there is an opportunity to
              create a great platform that brings people together with one eye
              on the future and think likely what that be might look like.
            </p>
          </Col>

          <Col lg="3" md="6" xs="6">
            <h4>
              <strong>Download App </strong>
            </h4>
            <Link
              to="/"
              className="features-os d-inline-flex align-items-center"
            >
              <img
                className="icon-store"
                src={googlePlay}
                alt="google play"
                width="80%"
                style={{ paddingTop: "12px" }}
              />
            </Link>

            <Link
              to="/"
              className="features-os d-inline-flex align-items-center"
            >
              <img
                className="icon-store appStore"
                src={appStore}
                alt="appStore"
                width="80%"
                style={{ paddingTop: "12px" }}
              />
            </Link>
          </Col>
          <Col lg="3" md="6" xs="6" className="sosmed ">
            <h4>
              <strong>Social media</strong>
            </h4>
            <Link to="/">
              <img
                className="icon-sosmed"
                src={facebook}
                alt="facebook"
                width="80px"
              />
            </Link>
            <Link to="/">
              <img
                className="icon-sosmed"
                src={instagram}
                alt="instagram"
                width="80px"
              />
            </Link>
            <Link to="/">
              <img
                className="icon-sosmed"
                src={pinterest}
                alt="pinterest"
                width="80px"
              />
            </Link>
            <Link to="/">
              <img
                className="icon-sosmed"
                src={youtube}
                alt="youtube"
                width="80px"
              />
            </Link>
            <Row>
              <Link to="/" className="features about">
                <h4>
                  <strong>About</strong>
                </h4>
              </Link>
              <Link to="/" className="features">
                <h4>
                  <strong>FAQ</strong>
                </h4>
              </Link>
            </Row>
          </Col>
        </Row>
        <hr style={{ border: "1px solid #545454" }} />
        <Row className="sub-footer d-flex justify-content-center ">
          <Col>
            <h5 style={{ textAlign: "center" }}>
              Copyright © 2020 <strong>KICKIN Batch 8 </strong>Glints Academy &
              Binar Academy
            </h5>
          </Col>
        </Row>
        <br />
      </Container>
    </div>
  );
};

export default Footer;

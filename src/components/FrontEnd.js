import React from "react";
import "../styles/FrontEnd.css";
import frontend1 from "../img/frontend-1.jpeg";
import frontend2 from "../img/frontend-2.jpeg";
import frontend3 from "../img/frontend-3.jpg";
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardSubtitle,
} from "reactstrap";

import linkedin from "../icon/linkedin.png";
import gitlab from "../icon/gitlab.png";

const FrontEnd = () => {
  return (
    <>
      <Container className="frontEnd">
        <Row>
          <Col lg="4" sm="4" className="sosmed ">
            <Card className="card-limit">
              <CardImg top width="100%" src={frontend1} alt="FE-1" />
              <CardBody>
                <CardTitle tag="h5">Ari Darsan</CardTitle>
                <CardSubtitle tag="h6" className="mb-2">
                  Front End
                </CardSubtitle>
                <a
                  href="https://www.linkedin.com/in/ari-darsan-7100181b6/"
                  target="_blank"
                  rel="noreferrer"
                  className="features-os d-inline-flex align-items-center"
                >
                  <img
                    className="icon-store appStore"
                    src={linkedin}
                    alt="linkedin"
                    height="70px"
                    style={{ paddingTop: "12px" }}
                  />
                </a>
                <a
                  href="https://gitlab.com/aridarsan"
                  target="_blank"
                  rel="noreferrer"
                  className="features-os d-inline-flex align-items-center"
                >
                  <img
                    className="icon-store appStore"
                    src={gitlab}
                    alt="gitlab"
                    height="70px"
                    style={{ paddingTop: "12px" }}
                  />
                </a>
              </CardBody>
            </Card>
          </Col>
          <Col lg="4" sm="4" className="sosmed ">
            <Card className="card-limit">
              <CardImg top width="100%" src={frontend2} alt="FE-2" />
              <CardBody>
                <CardTitle tag="h5">Satrio Kamaludin</CardTitle>
                <CardSubtitle tag="h6" className="mb-2">
                  Front End
                </CardSubtitle>
                <a
                  href="https://www.linkedin.com/in/satrio-kamaludin-105a9a14a/"
                  target="_blank"
                  rel="noreferrer"
                  className="features-os d-inline-flex align-items-center"
                >
                  <img
                    className="icon-store appStore"
                    src={linkedin}
                    alt="linkedin"
                    height="70px"
                    style={{ paddingTop: "12px" }}
                  />
                </a>
                <a
                  href="https://gitlab.com/satriokamaludin"
                  target="_blank"
                  rel="noreferrer"
                  className="features-os d-inline-flex align-items-center"
                >
                  <img
                    className="icon-store appStore"
                    src={gitlab}
                    alt="gitlab"
                    height="70px"
                    style={{ paddingTop: "12px" }}
                  />
                </a>
              </CardBody>
            </Card>
          </Col>
          <Col lg="4" sm="4" className="sosmed ">
            <Card className="card-limit">
              <CardImg top width="100%" src={frontend3} alt="FE-3" />
              <CardBody>
                <CardTitle tag="h5">Deswanto Damanik</CardTitle>
                <CardSubtitle tag="h6" className="mb-2">
                  Front End
                </CardSubtitle>
                <a
                  href="https://www.linkedin.com/in/deswandamanik/"
                  target="_blank"
                  rel="noreferrer"
                  className="features-os d-inline-flex align-items-center"
                >
                  <img
                    className="icon-store appStore"
                    src={linkedin}
                    alt="linkedin"
                    height="70px"
                    style={{ paddingTop: "12px" }}
                  />
                </a>
                <a
                  href="https://gitlab.com/deswandamanik"
                  target="_blank"
                  rel="noreferrer"
                  className="features-os d-inline-flex align-items-center"
                >
                  <img
                    className="icon-store appStore"
                    src={gitlab}
                    alt="gitlab"
                    height="70px"
                    style={{ paddingTop: "12px" }}
                  />
                </a>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default FrontEnd;

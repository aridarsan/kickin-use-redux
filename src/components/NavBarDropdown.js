import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import axios from "axios";
import "@fortawesome/fontawesome-free";
import {
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Spinner,
  Nav,
  Row,
} from "reactstrap";
import avatardef from "../icon/avatar2.png";
// import { useGoogleLogout } from "react-google-login";
// const clientId =
//   "813981448798-5shmuqt19u305tokvtmspmsaa93s2h9o.apps.googleusercontent.com";

const NavBarDropdown = (props) => {
  const [dropdownOpen, setOpen] = useState(false);
  const [userData, setUserData] = useState([]);

  // const baseUrl = "http://kickin.southeastasia.cloudapp.azure.com";
  const baseUrl = "https://binar8-novien-ghoziana.nandaworks.com"
  // const baseUrl = "https://kickin-app.herokuapp.com";

  const token = localStorage.getItem("token");
  const id = localStorage.getItem("id");

  useEffect(() => {
    var config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    };

    axios
      .get(`${baseUrl}/profile?id=${id}`, config)
      .then((res) => {
        setUserData(res.data);
      })
      .catch((err) => console.log(err));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleLogout = () => {
    localStorage.clear();
    // signOut();
    props.setIsLogin(false);
  };
  const toggle = () => setOpen(!dropdownOpen);

  // console.log(props.isLogin)

  //logout google
  // const onLogoutSuccess = (res) => {
  //   console.log("Logged out Success");
  //   // alert("Logged out Successfully ✌");
  // };

  // const onFailure = () => {
  //   console.log("Handle failure cases");
  // };

  // const { signOut } = useGoogleLogout({
  //   clientId,
  //   onLogoutSuccess,
  //   onFailure,
  // });

  return (
    props.isLogin === true &&
    (userData ? (
      <Row>
        {userData.map((user) => (
          <>
            <ButtonDropdown
              className="profile-toggle"
              isOpen={dropdownOpen}
              toggle={toggle}
            >
            <Nav className="mt-3 mb-2 ml-3" style={{padding:"0 15px"}}>
              <h5> Welcome, {user.fullname}</h5>
            </Nav>
              <DropdownToggle
                color="#313131"
                style={{
                  float: "right",
                  backgroundColor: "#222222",
                  borderRadius: "30px",
                  width: "50px",
                }}
                caret
                size="xs"
                className="mr-2"
              >
                <img
                  src={user.photo === null ? avatardef : user.photo}
                  alt="pp"
                  className="avatar"
                  style={{ border: "2px solid #53c9c2" }}
                />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem className="profil-drop">
                  <NavLink
                    to="/profile"
                    activeClassName="active"
                    className="nav-link m-auto"
                    color="white"
                  >
                    <h5>
                      <i class="fas fa-user-alt mr-2"></i>Profile
                    </h5>
                  </NavLink>
                </DropdownItem>

                <DropdownItem>
                  <NavLink
                    to="/about/all"
                    activeClassName="active"
                    className="nav-link"
                    color="white"
                  >
                    <h5>
                      <i class="fas fa-info-circle mr-2"></i>Our Team
                    </h5>
                  </NavLink>
                </DropdownItem>

                <DropdownItem>
                  <NavLink
                    to="/"
                    onClick={handleLogout}
                    activeClassName="active"
                    className="nav-link"
                    color="white"
                  >
                    <h5>
                      <i class="fas fa-sign-out-alt mr-2"></i>Logout
                    </h5>
                  </NavLink>
                </DropdownItem>
              </DropdownMenu>
            </ButtonDropdown>
          </>
        ))}
      </Row>
    ) : (
      <Spinner color="primary" />
    ))
  );
};

export default NavBarDropdown;

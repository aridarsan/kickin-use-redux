import React, { useState, useEffect } from "react";
import axios from "axios";
import moment from "moment";
import { Table, Button, Modal, Col, Row, Collapse, ModalBody } from "reactstrap";
import "../styles/ViewTicket.css";

const ViewTicket = (props) => {
  const [modalTicket, setModalTicket] = useState(false);
  const [ticket, setTicket] = useState("");
  const [isOpen, setIsOpen] = useState(false);
  const toggleTicket = () => setModalTicket(!modalTicket);
  const toggle = () => setIsOpen(!isOpen);

  const baseUrl = "https://binar8-novien-ghoziana.nandaworks.com";
  const token = localStorage.getItem("token");

  //authorization
  var config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + token,
    },
  };

  useEffect(() => {
    getTicket();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getTicket = () => {
    axios
      .get(
        `${baseUrl}/ticket/read?q=${props.id}`,
        config
      )
      .then((res) => {
        setTicket(res.data[0]);
        // console.log(res.data[0]);
      })
      // .catch((err) => console.log(err));
  };
  // console.log(props.id)

  return (
    <>
    {ticket.length !== 0 ? (
    <>
      <Button
        onClick={toggleTicket}
        color="success"
        className="direction mt-3 mb-3"
      >
        <i class="fas fa-ticket-alt mr-2"></i>
        <strong>TICKET</strong>
      </Button>
      <Modal
        isOpen={modalTicket}
        toggle={toggleTicket}
        className="custom-modal-size"
      >
        <div className="modal-body">
          <Row>
            <Col>
              <Button onClick={toggleTicket} close aria-label="Cancel">
                <span aria-hidden>
                  <i class="fas fa-times-circle"></i>
                </span>
              </Button>
            </Col>
          </Row>
          <div style={{ textAlign: "center" }}>
            <h3 className="modal-title">Ticket</h3>
            <hr />
            <text className="ticketcode">{ticket.id.slice(0, 8)}</text>
            <hr />
          </div>
          <Table className="table-borderless">
            <tr>
              <th scope="row" >Field Name</th>
              <td>{ticket.fieldTime.field.name}</td>
            </tr>
            <tr>
              <th scope="row">Field Address</th>
              <td>{ticket.fieldTime.field.address}</td>
            </tr>
            <tr>
              <th scope="row">Date & Time</th>
              <td>
                {moment(ticket.startTime).format("D MMM YYYY, k:mm")}-{" "}
                {moment(ticket.endTime).format("k:mm")}
              </td>
            </tr>
            <tr>
              <th scope="row">Booked By</th>
              <td>
                {ticket.user.fullname}
              </td>
            </tr>
          </Table>
          <Button className="btn btn-primary" onClick={toggleTicket}>
            OK
          </Button>
          <Button
            className="btn-second"
            block
            onClick={toggle}
            style={{ marginTop: "1rem", marginBottom: "1rem" }}
          >
            How to use you're ticket ?
          </Button>
          <Collapse isOpen={isOpen}>
            <p>1. Visit the futsal field that you choose</p>
            <p>
              2. Show this ticket to the counter at least 5 minute before the
              game
            </p>
            <p>
              3. If do you want to share this ticket to your friends, you only
              need to screenshot this ticket
            </p>
            <p>
              4. Our counter will check your booking code so make sure keep the
              booking code protected.
            </p>
            <p>5. You're ready to Kick</p>
          </Collapse>
        </div>
      </Modal>
      </>
    )
      : (
        <div>
        <ModalBody
          style={{
            width: "400px",
            height: "300px",
            margin: "50px",
            padding: "0px",
          }}
        >
          <Row>
            <Col>
              <Button onClick={toggleTicket} close aria-label="Cancel">
                <span aria-hidden>
                  <i class="fas fa-times-circle"></i>
                </span>
              </Button>
            </Col>
          </Row>

          <Col>
            <h4 style={{ textAlign: "center", color: "#53c9c2" }}>
              Hello User <br />
              you have'nt confirmation your payment
            </h4>
            <h5 style={{ textAlign: "center" }}>
              if you want view ticket please confirmation payment first
            </h5>
          </Col>
        </ModalBody>
      </div>
      )}
    </>
  );
};

export default ViewTicket;

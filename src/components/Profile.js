//package
import React, { useEffect, useState } from "react";
import {
  Container,
  Button,
  Row,
  Col,
  Card,
  CardBody,
  Tooltip,
} from "reactstrap";
import { Rating } from "@material-ui/lab";
import { Redirect } from "react-router-dom";
import axios from "axios";
import moment from "moment";
import EditProfile from "./EditProfile";
import avatardef from "../icon/avatar2.png";
import Loading from "./Loading";
//modal
import Feedback from "./Feedback";
import EditFeedback from "./EditFeedback";
import PaymentConfirmation from "./PaymentConfirmation";
import ViewTicket from "./ViewTicket";
import swal from "sweetalert";
//styling
import "@fortawesome/fontawesome-free";
import "../styles/Profile.css";
const Profile = (props) => {
  //Data
  const [userData, setUserData] = useState([]);
  const [userBooking, setUserBooking] = useState("");
  const [userReview, setUserReview] = useState([]);

  // const baseUrl = "http://kickin.southeastasia.cloudapp.azure.com";
  const baseUrl = "https://binar8-novien-ghoziana.nandaworks.com";
  const token = localStorage.getItem("token");
  const id = localStorage.getItem("id");

  //canceled
  const [tooltipOpen, setTooltipOpen] = useState(false);
  const toggle = () => setTooltipOpen(!tooltipOpen);
  //waiting
  const [tooltipOpenWait, setTooltipOpenWait] = useState(false);
  const toggleWait = () => setTooltipOpenWait(!tooltipOpenWait);
  //decline
  const [tooltipOpenDecline, setTooltipOpenDecline] = useState(false);
  const toggleDecline = () => setTooltipOpenDecline(!tooltipOpenDecline);

  //authorization
  var config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + token,
    },
  };

  //Get User Profile
  const getUserProfile = () => {
    axios
      .get(`${baseUrl}/profile?id=${id}`, config)
      .then((res) => {
        // console.log(res.data);
        setUserData(res.data[0]);
      })
      // .catch((err) => console.log(err));
  };

  //Book History
  const getBookHistory = () => {
    axios
      .get(`${baseUrl}/booking/user/list?id=${id}`, config)
      .then((res) => {
        setUserBooking(res.data.data.getUserBooking);
        // console.log("idi data user booking", res.data.data.getUserBooking);
      })
      // .catch((err) => console.log(err));
  };

  //Get User Review
  const getUserReview = () => {
    axios
      .get(`${baseUrl}/review/read/${id}`, config)
      .then((res) => {
        setUserReview(res.data);
        // console.log(res.data)
      })
      // .catch((err) => {
      //   console.log(err);
      // });
  };

  useEffect(() => {
    getUserProfile();
    getUserReview();
    getBookHistory();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (token === null) {
    swal({
      icon: "warning",
      title: "Hello Guest",
      text: "please login first to get this page",
      type: "warning",
      buttons: false,
      timer: 3000,
    });
    return <Redirect to="/" />;
  }

  return (
    <>
      <div>
        <Container className="Profile">
          <Row>
            {/* User Profil */}
            <Col md={12} lg={4} xs={12}>
              <div>
                <Col
                  style={{
                    border: "1px solid #7b7b7b",
                    borderRadius: "10px",
                  }}
                >
                  <Col style={{ margin: "20px auto" }}>
                    <h3 className="bio" style={{ textAlign: "center" }}>
                      User Profil
                    </h3>
                    <br />
                    <img
                      className="profil-picture"
                      src={userData.photo === null ? avatardef : userData.photo}
                      alt="logo"
                    />
                    <h3 className="fullname">{userData.fullname}</h3>
                    <h5>Email : {userData.email}</h5>
                    <h5>
                      Phone :{" "}
                      {userData.phoneNumber === null
                        ? "-"
                        : userData.phoneNumber}
                    </h5>
                    <h4 className="bio">Bio</h4>
                    <p>{userData.bio}</p>
                    <br />

                    <EditProfile
                      bio={userData.bio}
                      fullname={userData.fullname}
                      email={userData.email}
                      phone={userData.phone}
                      photo={userData.photo}
                    />
                  </Col>
                </Col>
              </div>

              <br />

              {/* User Review */}
              <Col
                className="mb-3"
                style={{
                  border: "1px solid #7b7b7b",
                  borderRadius: "10px",
                }}
              >
                <h3 className="mt-3 ml-3">My Review</h3>
                <Col style={{ margin: "20px auto" }}>
                  {userReview ? (
                    userReview.length !== 0 ? (
                      userReview.map((review) => (
                        <div>
                          <Card
                            className="col-sm-12 mb-3 mt-3"
                            style={{
                              backgroundColor: "#313131",
                              border: "none",
                            }}
                          >
                            <CardBody>
                              <h4 className="review-title">
                                <b>{review.field.name}</b>
                              </h4>
                              <Rating
                                name="half-rating-read"
                                defaultValue={review.rating}
                                precision={0.2}
                                max={5}
                                readOnly
                              />

                              <p className="review-p">{review.comment}</p>

                              <p style={{ color: "#53c9c2" }}>
                                <em>
                                  {moment(review.createdAt).format(
                                    "D MMM YYYY, kk:mm "
                                  )}
                                </em>
                              </p>

                              <EditFeedback
                                comment={review.comment}
                                rating={review.rating}
                                id={review.id}
                              />
                            </CardBody>
                          </Card>
                        </div>
                      ))
                    ) : (
                      <div>
                        <h5 style={{ textAlign: "center" }}>No feedback yet</h5>
                        <br />
                      </div>
                    )
                  ) : (
                    <Loading />
                  )}
                </Col>
              </Col>
            </Col>

            {/* User Booking */}
            <Col>
              <div className="book-history">
                <h3 className="h4-book-history">Book History</h3>
                {userBooking ? (
                  userBooking.length !== 0 ? (
                    <div>
                      {userBooking.map((booking) => (
                        <div>
                          <div className="history">
                            <br />
                            {new Date(booking.startTime).getTime() >
                            new Date(moment()).getTime() ? (
                              <h4 className="comingupmatch">Coming up Match</h4>
                            ) : (
                              <h4
                                className="comingupmatch"
                                style={{ color: "#53c9c2" }}
                              >
                                Passed Match
                              </h4>
                            )}
                            <Row className="fielddate mt-3">
                              <Col className="fielddate">
                                <h4 className="fieldname">
                                  {booking.fieldTime.field.name}
                                </h4>
                              </Col>
                              <Col className="fielddate">
                                <h4 className="fieldname">
                                  {moment(booking.startTime).format(
                                    "D MMM YYYY, kk:mm "
                                  )}
                                </h4>
                              </Col>
                            </Row>
                            <a
                              href={booking.fieldTime.field.addressUrl}
                              target="blank_"
                            >
                              <Button className="direction">
                                <i class="fas fa-location-arrow mr-2"></i>
                                <strong>DIRECTION</strong>
                              </Button>
                            </a>
                            <br />

                            {booking.status === "accepted" ? (
                              new Date(booking.startTime).getTime() <
                              new Date(moment()).getTime() ? (
                                <Feedback
                                  fieldId={booking.fieldTime.field.id}
                                  isLoading={props.isLoading}
                                  setIsLoading={props.setIsLoading}
                                />
                              ) : (
                                <ViewTicket id={booking.id} />
                              )
                            ) : booking.status ===
                              "waiting for confirmation" ? (
                              <>
                                <Button
                                  id="waiting"
                                  color="info"
                                  className="direction mt-3 mb-3"
                                >
                                  <i class="fas fa-check mr-2"></i>
                                  Waiting for Confirmation
                                </Button>
                                <div>
                                  <Tooltip
                                    placement="bottom"
                                    isOpen={tooltipOpenWait}
                                    target="waiting"
                                    toggle={toggleWait}
                                    autohide={false}
                                  >
                                    <p>
                                      We are currently processing your payment.
                                      please wait for a while.
                                    </p>
                                  </Tooltip>
                                </div>
                              </>
                            ) : booking.status === "waiting for payment" ? (
                              <PaymentConfirmation
                                id={booking.id}
                                transactionId={booking.transactions[0]}
                                name={booking.fieldTime.field.name}
                              />
                            ) : booking.status === "decline" ? (
                              <>
                                <Button
                                  id="decline"
                                  color="danger"
                                  className="direction mt-3 mb-3"
                                >
                                  <i class="fas fa-ban mr-2"></i>
                                  <strong>DECLINED</strong>
                                </Button>
                                <div>
                                  <Tooltip
                                    placement="bottom"
                                    isOpen={tooltipOpenDecline}
                                    target="decline"
                                    toggle={toggleDecline}
                                    autohide={false}
                                  >
                                    <p>
                                      Your Booking has been declined due we did
                                      not receive your transaction payment proof
                                      within 24 hours, please book again
                                    </p>
                                  </Tooltip>
                                </div>
                              </>
                            ) : (
                              <>
                                <Button
                                  id="canceled"
                                  color="danger"
                                  className="direction mt-3 mb-3"
                                >
                                  <i class="fas fa-ban mr-2"></i>
                                  <strong>CANCELED</strong>
                                </Button>
                                <div>
                                  <Tooltip
                                    placement="bottom"
                                    isOpen={tooltipOpen}
                                    target="canceled"
                                    toggle={toggle}
                                    autohide={false}
                                  >
                                    <p>
                                      Your Booking has been canceled by admin or
                                      user. please book again
                                    </p>
                                  </Tooltip>
                                </div>
                              </>
                            )}
                          </div>

                          <br />
                        </div>
                      ))}
                    </div>
                  ) : (
                    <Col>
                      <div style={{ minWidth: "200px" }}>
                        <h5 style={{ textAlign: "center" }}>
                          no booking field data
                        </h5>
                        <br />
                      </div>
                    </Col>
                  )
                ) : (
                  <Loading />
                )}
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default Profile;

import React from "react";
import "../styles/Mobile.css";
import mobile1 from "../img/mobile-1.jpeg";
import mobile2 from "../img/mobile-2.jpeg";

import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardSubtitle,
} from "reactstrap";

import linkedin from "../icon/linkedin.png";
import gitlab from "../icon/gitlab.png";

function Mobile() {
  return (
    <>
      <Container className="mobile">
        <Row>
          <Col sm="4">
            <Card className="card-limit">
              <CardImg top width="100%" src={mobile1} alt="RN-1" />
              <CardBody>
                <CardTitle tag="h5">Chandra Santoso</CardTitle>
                <CardSubtitle tag="h6" className="mb-2">
                  React Native
                </CardSubtitle>
                <a
                  href="https://www.linkedin.com/in/chansosemar/"
                  target="_blank"
                  rel="noreferrer"
                  className="features-os d-inline-flex align-items-center"
                >
                  <img
                    className="icon-store appStore"
                    src={linkedin}
                    alt="linkedin"
                    height="70px"
                    style={{ paddingTop: "12px" }}
                  />
                </a>
                <a
                  href="https://gitlab.com/chansosemar"
                  target="_blank"
                  rel="noreferrer"
                  className="features-os d-inline-flex align-items-center"
                >
                  <img
                    className="icon-store appStore"
                    src={gitlab}
                    alt="gitlab"
                    height="70px"
                    style={{ paddingTop: "12px" }}
                  />
                </a>
              </CardBody>
            </Card>
          </Col>
          <Col sm="4">
            <Card className="card-limit">
              <CardImg top width="100%" src={mobile2} alt="RN-2" />
              <CardBody>
                <CardTitle tag="h5">David Soerdjana</CardTitle>
                <CardSubtitle tag="h6" className="mb-2">
                  React Native
                </CardSubtitle>
                <a
                  href="https://www.linkedin.com/in/david-surjana-957488155/"
                  target="_blank"
                  rel="noreferrer"
                  className="features-os d-inline-flex align-items-center"
                >
                  <img
                    className="icon-store appStore"
                    src={linkedin}
                    alt="linkedin"
                    height="70px"
                    style={{ paddingTop: "12px" }}
                  />
                </a>
                <a
                  href="http://gitlab.com/davidsoerdjana"
                  target="_blank"
                  rel="noreferrer"
                  className="features-os d-inline-flex align-items-center"
                >
                  <img
                    className="icon-store appStore"
                    src={gitlab}
                    alt="gitlab"
                    height="70px"
                    style={{ paddingTop: "12px" }}
                  />
                </a>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default Mobile;

import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import {
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  Button,
  Row,
  Col,
  Container,
  FormGroup,
  Input,
  CardSubtitle,
  Form,
  InputGroup,
  InputGroupAddon,
} from "reactstrap";
import swal from "sweetalert";
import Loading from "./Loading";
import ModalBookingField from "./ModalBookingField";
import "@fortawesome/fontawesome-free";
import "../styles/BrowseField.css";

const BrowseField = (props) => {
  // const [dropdownOpenFilter, setOpenFilter] = useState(false);
  const [dropdownOpenSort, setOpenSort] = useState(false);

  // const toggleFilter = () => setOpenFilter(!dropdownOpenFilter);
  const toggleSort = () => setOpenSort(!dropdownOpenSort);

  const [fieldss, setFieldss] = useState([]);
  const [searchLocation, setSearchLocation] = useState("");
  const [searchName, setSearchName] = useState("");
  const [loading, setLoading] = useState(false);

  const history = useHistory();

  // const url = "http://kickin.southeastasia.cloudapp.azure.com";
  const url = "https://binar8-novien-ghoziana.nandaworks.com";
  // const url = "https://kickin-app.herokuapp.com"

  useEffect(() => {
    axios
      .get(url + "/fields/read/all")
      .then((res) => {
        setFieldss(res.data);
        // console.log(fieldss);
        setLoading(true);
      })
      .catch((err) => {
        console.log(err)
        swal({
          icon: "warning",
          title: "Failed to load data",
          text: "Please wait",
          type: "warning",
          buttons: false,
          timer: 3000,
        });
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fieldSearchLocation = (e) => {
    e.preventDefault();
    axios
      .get(`${url}/fields/location?q=${searchLocation}`)
      .then((res) => {
        setFieldss(res.data[0].fields);
        // console.log(res.data[0]);
        setLoading(true);
      })
      .catch((err) => {
        // console.log(err);
        swal({
          icon: "warning",
          title: "Sorry, there is no data",
          text: "Please try again with another location",
          type: "warning",
          buttons: false,
          timer: 3000,
        });
      });
  };

  const fieldSearchName = (e) => {
    e.preventDefault();
    axios
      .get(`${url}/fields/name?q=${searchName}`)
      .then((res) => {
        setLoading(true);
        setFieldss(res.data);
        // console.log(res.data);
      })
      .catch((err) => {
        console.log(err)
        swal({
          icon: "warning",
          title: "Sorry, there is no data",
          text: "Please try again with another name",
          type: "warning",
          buttons: false,
          timer: 3000,
        });
      });
  };

  const sortByName = (e) => {
    e.preventDefault();
    axios
      .get(`${url}/fields/sort/name`)
      .then((res) => {
        setFieldss(res.data);
        // console.log(res.data);
        setLoading(true);
      })
      .catch((err) => {
        // console.log(err);
        swal({
          icon: "warning",
          title: "Sorry, there is no data",
          text: "Please select another name",
          type: "warning",
          buttons: false,
          timer: 3000,
        });
      });
  };

  const sortByPrice = (e) => {
    e.preventDefault();
    axios
      .get(`${url}/fields/sort/price`)
      .then((res) => {
        setFieldss(res.data);
        // console.log(res.data);
        setLoading(true);
      })
      .catch((err) => {
        // console.log(err);
        swal({
          icon: "warning",
          title: "Sorry, there is no data",
          text: "Please select another price",
          type: "warning",
          buttons: false,
          timer: 3000,
        });
      });
  };

  return (
    <>
      <Container className="search">
        <Row>
          <Col xs="6" sm="4" className="col-2">
            <h3 style={{ padding: "15px 0" }}>Browse Field</h3>
          </Col>

          <Col xs="6" sm="2" className="col-2">
            <ButtonDropdown isOpen={dropdownOpenSort} toggle={toggleSort}>
              <DropdownToggle block className="sort ml-auto" caret>
                Sort By
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem onClick={sortByName}>Name Ascending</DropdownItem>
                <DropdownItem onClick={sortByPrice}>
                  Price Ascending
                </DropdownItem>
              </DropdownMenu>
            </ButtonDropdown>
          </Col>

          <Col xs="6" sm="3" className="col-2">
            <Form onSubmit={fieldSearchLocation}>
              <FormGroup>
                <InputGroup>
                  <Input
                    xs="12"
                    type="text"
                    id="searchLocation"
                    name="searchLocation"
                    onChange={(e) => setSearchLocation(e.target.value)}
                    placeholder="Search location"
                  />
                  <InputGroupAddon addonType="append">
                    <Button className="btn-primary">
                      <i class="fas fa-search"></i>
                    </Button>
                  </InputGroupAddon>
                </InputGroup>
              </FormGroup>
            </Form>
          </Col>

          <Col xs="6" sm="3" className="col-2">
            <Form onSubmit={fieldSearchName}>
              <FormGroup>
                <InputGroup>
                  <Input
                    xs="12"
                    type="text"
                    id="searchLocation"
                    name="searchLocation"
                    onChange={(e) => setSearchName(e.target.value)}
                    placeholder="Search field"
                  ></Input>
                  <InputGroupAddon addonType="append">
                    <Button className="btn-primary">
                      <i class="fas fa-search"></i>
                    </Button>
                  </InputGroupAddon>
                </InputGroup>
              </FormGroup>
            </Form>
          </Col>
        </Row>
      </Container>

      <div className="fields">
        <Container>
          <Row className="text-white mt-3">
            {fieldss && loading ? (
              fieldss.map((field) => (
                <Col md={6} lg={3} className="class-col">
                  <Card className="card-field">
                    {field.fieldPhotos.slice(1, 2).map((photo) => (
                      <CardImg
                        className="card-img"
                        top
                        width="100%"
                        src={photo.photoUrl}
                        alt="photo field"
                      />
                    ))}

                    <CardBody className="card-body">
                      <CardTitle tag="h4">{field.name}</CardTitle>
                      <CardSubtitle tag="h5" className="mt-0 ml-auto">
                        Rp. {field.price}
                        <i class="fas fa-tag ml-2"></i>
                      </CardSubtitle>
                      <CardText className="mt-2">
                        <i class="fas fa-map-marker-alt"></i>
                        {field.address}
                      </CardText>
                      <Row style={{ padding: "0 16px" }}>
                        <Link to="#" className="col-5 view">
                          <Button
                            block
                            className="btn-second"
                            onClick={() => {
                              history.push(`/field-detail/${field.id}`);
                            }}
                          >
                            <i class="fas fa-eye mr-1"></i>
                            View
                          </Button>
                        </Link>

                        <Link to="#" className="col-5 view ml-auto">
                          <ModalBookingField
                            name={field.name}
                            id={field.id}
                            isLogin={props.isLogin}
                            setIsLogin={props.setIsLogin}
                            price={field.price}
                          />
                        </Link>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
              ))
            ) : (
              <Loading />
            )}
          </Row>
        </Container>
      </div>
    </>
  );
};

export default BrowseField;

import React from "react";
import FrontEnd from "./FrontEnd";
import BackEnd from "./BackEnd";
import Mobile from "./Mobile";

const All = () => {
    return(
    <>
        <FrontEnd />
        <BackEnd />
        <Mobile />
    </>
    )
}

export default All;
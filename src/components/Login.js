/* eslint-disable no-useless-escape */
import React, { useState } from "react";
import { Link } from "react-router-dom";
// import { useGoogleLogin } from "react-google-login";
// import axios from "axios";
import swal from "sweetalert";
import "@fortawesome/fontawesome-free";
import "../styles/Auth.css";
import {
  Button,
  Modal,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
  Spinner,
  Alert,
  Col,
  Row,
} from "reactstrap";
import { useDispatch } from "react-redux";

const Login = (props) => {
  // const clientId =
  //   "813981448798-5shmuqt19u305tokvtmspmsaa93s2h9o.apps.googleusercontent.com";
  //Define Function
  // const urlLogin = "http://kickin.southeastasia.cloudapp.azure.com/auth/login";
  // const urlLogin = "https://binar8-novien-ghoziana.nandaworks.com/auth/login";
  // const urlGoogleLogin =
  //   "http://kickin.southeastasia.cloudapp.azure.com/auth/login/google";

  const dispatch = useDispatch()
  const { className } = props;

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState(null);
  const [isSubmitting, setIsSubmitting] = useState(false);
  // const [googleToken, setGoogleToken] = useState();
  const [modalLogin, setModalLogin] = useState(false);
  const toggleLogin = () => setModalLogin(!modalLogin);

  const checkerLogin = () => {
    //Password and Email Formatting
    let mailformat = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
    // let passwordformat = /^(?=.*[0-9])(?=.*[A-Z]).{8,32}$/g;

    if (!email) {
      setMessage("Email Must be Filled!");
      return false;
    } else if (!email.match(mailformat)) {
      setMessage("Email Invalid!");
      return false;
    } else {
      setIsSubmitting(true);
    }
    // else if (!password) {
    //   setMessage("Password Must be Filledd!");
    //   return false;
    // } else if (!password.match(passwordformat)) {
    //   setMessage("Password Invalid!");
    //   return false;
    // }
  };

  //Submit Action
  const handleSubmit = (e) => {
    e.preventDefault();
    if (checkerLogin() !== false) {
      const data = {
        email: email,
        password: password,
      };
      dispatch({type: 'LOGIN', payload: data});

      //Post Data
      // axios
      //   .post(urlLogin, data)
      //   .then((res) => {
      //     const { id, token, role } = res.data;
      //     localStorage.setItem("id", id);
      //     localStorage.setItem("token", token);
      //     localStorage.setItem("role", role);
      //     setModalLogin(false);
      //     // console.log(res.data);
      //     props.setIsLogin(true);
      //     res.data.role === "admin"
      //     ? history.push("/admin")
      //     : history.push("/");
      //     swal({
      //       icon: "success",
      //       title: "Success Login",
      //       text: "let's book a field",
      //       type: "success",
      //       buttons: false,
      //       timer: 3000,
      //     });
      //   })
      //   .catch((err) => {
      //     // console.log(err);
      //     setIsSubmitting(false);
      //     swal({
      //       icon: "error",
      //       title: "Wrong email or password",
      //       text: "please try again",
      //       type: "warning",
      //       buttons: false,
      //       timer: 2000,
      //     });
      //   });
    }
  };

  const handleSubmitGoogle = () => {
  //   const data = {
  //     token: googleToken,
  //   };
  //   //Post Data
  //   axios
  //     .post(urlGoogleLogin, data)
  //     .then((res) => {
  //       const { id, token } = res.data;
  //       localStorage.setItem("id", id);
  //       localStorage.setItem("token", token);
  //       setModalLogin(false);
  //       console.log(props);
  //       props.setIsLogin(true);
  //       history.push("/");
  //       swal({
  //         icon: "success",
  //         title: "Login successfully",
  //         text: "let's book a field",
  //         type: "success",
  //         buttons: false,
  //         timer: 3000,
  //       });
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //       setIsSubmitting(false);
        swal({
          icon: "error",
          title: "Failed to Login With Google",
          text: "please try with other way",
          type: "warning",
          buttons: false,
          timer: 2000,
        });
  //     });
  };


  // //google auth
  // const onSuccess = (res) => {
  //   console.log("Login Success: currentUser:", res.profileObj);
  //   setGoogleToken(res.tokenId);
  //   console.log(googleToken);
  //   if (googleToken != null) {
  //     handleSubmitGoogle();
  //   }
  //   console.log(res.tokenId);
  //   // alert(
  //   //   `Logged in successfully welcome ${res.profileObj.name} 😍.`
  //   // );
  // };

  // const onFailure = (res) => {
  //   console.log("Login failed: res:", res);
  //   swal({
  //     icon: "error",
  //     title: "Failed to Login With Google",
  //     text: "please try login with regular way",
  //     type: "warning",
  //     buttons: false,
  //     timer: 2000,
  //   });
  // };

  // const { signIn } = useGoogleLogin({
  //   onSuccess,
  //   onFailure,
  //   clientId,
  //   isSignedIn: true,
  //   accessType: "offline",
  //   // responseType: 'code',
  //   // prompt: 'consent',
  // });

  // console.log(googleToken)

  return (
    <div>
      <Link
        onClick={toggleLogin}
        style={{
          textDecoration: "none",
        }}
      >
        <h5 className="mb-sm-0 btn-login">Log In</h5>
      </Link>
      <Modal
        isOpen={modalLogin}
        toggle={toggleLogin}
        className={className}
        id="modal-login"
      >
        <ModalBody>
          <Row>
            <Col>
              <Button onClick={toggleLogin} close aria-label="Cancel">
                <span aria-hidden>
                  <i class="fas fa-times-circle"></i>
                </span>
              </Button>
            </Col>
          </Row>
          <h3 className="title">Log In</h3>
          <FormGroup>
            {message && <Alert color="danger">{message}</Alert>}
          </FormGroup>

          <Form onSubmit={handleSubmit}>
            <FormGroup>
              <Label for="email">Email</Label>
              <Input
                type="email"
                name="email"
                id="email"
                autocomplete="username"
                className="col-12 m-auto"
                onChange={(e) => setEmail(e.target.value)}
              />
            </FormGroup>

            <FormGroup>
              <Label for="password">Password</Label>
              <Input
                type="password"
                name="password"
                id="password"
                autocomplete="current-password"
                className="col-12 m-auto"
                onChange={(e) => setPassword(e.target.value)}
              />
            </FormGroup>

            <FormGroup>
              {isSubmitting ? (
                <Button className="col-12 btn-primary" type="submit" disabled>
                  <Spinner color="light" />
                </Button>
              ) : (
                <Button className="col-12 btn-primary" type="submit">
                  <i class="fas fa-sign-in-alt mr-2"></i>
                  <strong>Log In</strong>
                </Button>
              )}
            </FormGroup>
            <p className="or">Or</p>

            <FormGroup>
              <Button onClick={handleSubmitGoogle} className="col-12 btn-facebook">
                <i class="fab fa-google mr-2"></i>
                Log In with Google
              </Button>
            </FormGroup>
          </Form>
        </ModalBody>
      </Modal>
    </div>
  );
};

export default Login;

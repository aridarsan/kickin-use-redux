import axios from "axios";
import React, { useEffect, useState } from "react";
import { Redirect } from "react-router-dom";
import {
  FormGroup,
  Row,
  NavItem,
  TabPane,
  TabContent,
  Container,
  Nav,
  NavLink,
  Table,
  Button,
  Spinner,
} from "reactstrap";
import "../styles/Admin.css";
import classnames from "classnames";
import Loading from "./Loading";
import moment from "moment";
import swal from "sweetalert";

const Admin = () => {
  const baseUrl = "https://binar8-novien-ghoziana.nandaworks.com";
  const [dataBookingUser, setDataBookingUser] = useState([]);
  const [dataPayment, setDataPayment] = useState([]);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const token = localStorage.getItem("token");
  const role = localStorage.getItem("role");
  const [activeTab, setActiveTab] = useState("1");
  const toggletab = (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  var config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + token,
    },
  };

  const getUserPayment = () => {
    axios
      .get(`${baseUrl}/payment/admin/list`, config)
      .then((res) => {
        console.log(res.data.data.getpaymentListToday);
        setDataPayment(res.data.data.getpaymentListToday);
      })
      .catch((err) => console.log(err));
  };

  //Get User Profile
  const getUserBooking = () => {
    axios
      .get(`${baseUrl}/booking/admin/list`, config)
      .then((res) => {
        console.log(res.data.data.getBookingListToday);
        setDataBookingUser(res.data.data.getBookingListToday);
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    // setIsLoading(true);
    getUserBooking();
    getUserPayment();
    // setIsLoading(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);


  if (role !== "admin") {
    return <Redirect to="/" />;
  }
  return (
    <div>
      <div>
        <Container className="admin">
          <h2 className="bookinglisttitle">Admin Page</h2>
          <Container className="admintcontainer">
            <FormGroup>
              <Row className="mt-3">
                <Container>
                  <h3>Booking Confirmation</h3>
                </Container>
              </Row>
              <Nav tabs className="mt-3">
                <NavItem>
                  <NavLink
                    className={classnames({ activetab: activeTab === "1" })}
                    onClick={() => {
                      toggletab("1");
                    }}
                  >
                    Payment List User
                  </NavLink>
                </NavItem>

                <NavItem>
                  <NavLink
                    className={classnames({ activetab: activeTab === "2" })}
                    onClick={() => {
                      toggletab("2");
                    }}
                  >
                    Booking List User
                  </NavLink>
                </NavItem>
              </Nav>

              <TabContent activeTab={activeTab}>
                <TabPane tabId="1">
                  <Table className="admintable">
                    <thead>
                      <tr className="adminhead">
                        <th>
                          <h4>Total Payment</h4>
                        </th>
                        <th>
                          <h4>Photo</h4>
                        </th>
                        <th>
                          <h4>Date/Time</h4>
                        </th>
                        <th>
                          <h4>Status</h4>
                        </th>
                        <th colSpan="2">
                          <h4>Action</h4>
                        </th>
                        <th>
                          <h4>Booking Time</h4>
                        </th>
                      </tr>
                    </thead>

                    {dataPayment ? (
                      dataPayment !== 0 ? (
                        dataPayment.map((payment) => (
                          <tr className="adminrows">
                            <th className="adminth">
                              <div>Rp. {payment.transaction.totalPrice}</div>
                            </th>
                            <th className="adminth">
                              <a
                                href={payment.receiptPhoto}
                                alt="payment"
                                target="_blank"
                                rel="noreferrer"
                              >
                                {" "}
                                <Button color="warning" className="show">
                                  Show
                                </Button>
                              </a>
                            </th>
                            <th className="adminth">
                              <div>
                                {moment(
                                  payment.transaction.booking.startTime
                                ).format("D MMM YYYY, k:mm")}{" "}
                                -{" "}
                                {moment(
                                  payment.transaction.booking.endTime
                                ).format("k:mm")}
                              </div>
                            </th>
                            <th className="adminth">
                              {payment.transaction.booking.status ===
                              "waiting for confirmation" ? (
                                <Button
                                  block
                                  disabled
                                  color="success"
                                  className="paidaccepted"
                                >
                                  paid
                                </Button>
                              ) : payment.transaction.booking.status ===
                                "waiting for payment" ? (
                                <Button
                                  color="info"
                                  disabled
                                  className="paidaccepted"
                                >
                                  waiting
                                </Button>
                              ) : payment.transaction.booking.status ===
                                "decline" ? (
                                <Button
                                  disabled
                                  block
                                  color="danger"
                                  className="denied"
                                >
                                  declined
                                </Button>
                              ) : payment.transaction.booking.status ===
                                "canceled" ? (
                                <Button
                                  disabled
                                  block
                                  color="danger"
                                  className="denied"
                                >
                                  canceled
                                </Button>
                              ) : (
                                <Button
                                  block
                                  disabled
                                  color="success"
                                  className="paidaccepted"
                                >
                                  accepted
                                </Button>
                              )}
                            </th>

                            {payment.isAccepted === false ? (
                              <>
                                <th className="adminth">
                                  <Button
                                    onClick={(e) => {
                                      e.preventDefault();
                                      setIsSubmitting(true);
                                      const data = {
                                        isAccepted: true,
                                      };
                                      axios
                                        .patch(
                                          `${baseUrl}/payment/admin/list?id=${payment.id}`,
                                          data,
                                          config
                                        )
                                        .then((res) => {
                                          // console.log(res);
                                          swal({
                                            icon: "success",
                                            title: `Booking user has been accepted`,
                                            type: "success",
                                            buttons: false,
                                            timer: 3000,
                                          });
                                          setIsSubmitting(false);
                                        })
                                        .catch((err) => {
                                          console.log(err);
                                          swal({
                                            icon: "warning",
                                            title: "Failed to accept booking",
                                            text: "Please try again",
                                            type: "warning",
                                            buttons: false,
                                            timer: 3000,
                                          });
                                          setIsSubmitting(false);
                                        });
                                    }}
                                    color="info"
                                    className="accept"
                                  >
                                    {isSubmitting !== true ? (
                                      "Accept"
                                    ) : (
                                      <Spinner color="light" />
                                    )}
                                  </Button>
                                </th>
                                <th className="adminth">
                                  <Button
                                    onClick={(e) => {
                                      e.preventDefault();
                                      setIsSubmitting(true);
                                      const data = {
                                        status: "decline",
                                      };
                                      axios
                                        .patch(
                                          `${baseUrl}/payment/admin/list?id=${payment.id}`,
                                          data,
                                          config
                                        )
                                        .then((res) => {
                                          // console.log(res);
                                          swal({
                                            icon: "success",
                                            title: `Booking user has been declined`,
                                            type: "success",
                                            buttons: false,
                                            timer: 3000,
                                          });
                                          setIsSubmitting(false);
                                        })
                                        .catch((err) => {
                                          console.log(err);
                                          swal({
                                            icon: "warning",
                                            title: "Failed decline booking",
                                            text: "Please try again",
                                            type: "warning",
                                            buttons: false,
                                            timer: 3000,
                                          });
                                          setIsSubmitting(false);
                                        });
                                    }}
                                    color="danger"
                                    className="denied"
                                  >
                                    {isSubmitting !== true ? (
                                      "Decline"
                                    ) : (
                                      <Spinner color="light" />
                                    )}
                                  </Button>
                                </th>
                              </>
                            ) : (
                              <th colSpan="2">
                                <Button
                                  color="secondary"
                                  disabled
                                  className="accept"
                                >
                                  No action
                                </Button>
                              </th>
                            )}
                            <th className="adminth">
                              <div>
                                {moment(payment.createdAt).format(
                                  "D MMM YYYY, k:mm"
                                )}
                              </div>
                            </th>
                          </tr>
                        ))
                      ) : (
                        <tr>
                          <div>
                            <h5>There is no data booking from user </h5>
                          </div>
                        </tr>
                      )
                    ) : (
                      <Loading />
                    )}
                  </Table>
                </TabPane>

                <TabPane tabId="2">
                  <Table className="admintable">
                    <thead>
                      <tr className="adminhead">
                        <th>
                          <h4>Name</h4>
                        </th>
                        <th>
                          <h4>Field Name</h4>
                        </th>
                        <th>
                          <h4>Date/Time</h4>
                        </th>
                        <th>
                          <h4>Status</h4>
                        </th>
                        <th colSpan="2">
                          <h4>Action</h4>
                        </th>
                        <th>
                          <h4>Booking Time</h4>
                        </th>
                      </tr>
                    </thead>
                    {dataBookingUser ? (
                      dataBookingUser !== 0 ? (
                        dataBookingUser.map((booking) => (
                          <tr className="adminrows">
                            <th className="adminth">
                              <div>{booking.user.fullname}</div>
                            </th>
                            <th className="adminth">
                              <div>{booking.fieldTime.field.name}</div>
                            </th>
                            <th className="adminth">
                              <div>
                                {moment(booking.startTime).format(
                                  "D MMM YYYY, k:mm"
                                )}{" "}
                                - {moment(booking.endTime).format("k:mm")}
                              </div>
                            </th>
                            <th className="adminth">
                              {booking.status === "waiting for confirmation" ? (
                                <Button
                                  block
                                  color="success"
                                  className="paidaccepted"
                                >
                                  paid
                                </Button>
                              ) : booking.status === "waiting for payment" ? (
                                <Button className="paidaccepted">
                                  waiting
                                </Button>
                              ) : booking.status === "decline" ? (
                                <Button color="danger" className="denied">
                                  declined
                                </Button>
                              ) : booking.status === "canceled" ? (
                                <Button color="danger" className="denied">
                                  canceled
                                </Button>
                              ) : (
                                <Button
                                  color="success"
                                  className="paidaccepted"
                                >
                                  accepted
                                </Button>
                              )}
                            </th>

                            {booking.status === "waiting for confirmation" ? (
                              <>
                                <th className="adminth">
                                  <Button
                                    onClick={(e) => {
                                      e.preventDefault();
                                      setIsSubmitting(true);
                                      const data = {
                                        isSuccessful: true,
                                      };
                                      axios
                                        .patch(
                                          `${baseUrl}/booking/admin/list?id=${booking.id}`,
                                          data,
                                          config
                                        )
                                        .then((res) => {
                                          // console.log(res);
                                          swal({
                                            icon: "success",
                                            title: `Booking user has been accepted`,
                                            text:
                                              "let's waiting the new booking :)",
                                            type: "success",
                                            buttons: false,
                                            timer: 3000,
                                          });
                                          setIsSubmitting(false);
                                        })
                                        .catch((err) => {
                                          console.log(err);
                                          swal({
                                            icon: "warning",
                                            title: "Failed to accept booking",
                                            text: "Please try again",
                                            type: "warning",
                                            buttons: false,
                                            timer: 3000,
                                          });
                                          setIsSubmitting(false);
                                        });
                                    }}
                                    color="info"
                                    className="accept"
                                  >
                                    Accept
                                  </Button>
                                </th>
                                <th className="adminth">
                                  <Button
                                    onClick={(e) => {
                                      e.preventDefault();
                                      setIsSubmitting(true);
                                      const data = {
                                        status: "decline",
                                      };
                                      axios
                                        .patch(
                                          `${baseUrl}/booking/admin/list?id=${booking.id}`,
                                          data,
                                          config
                                        )
                                        .then((res) => {
                                          // setIsLoading(true)
                                          // console.log(res);
                                          swal({
                                            icon: "success",
                                            title: `Booking user has been declined`,
                                            text: "let's waiting the new booking :)",
                                            type: "success",
                                            buttons: false,
                                            timer: 3000,
                                          });
                                          setIsSubmitting(false);
                                        })
                                        .catch((err) => {
                                          console.log(err);
                                          swal({
                                            icon: "warning",
                                            title: "Failed decline booking",
                                            text: "Please try again",
                                            type: "warning",
                                            buttons: false,
                                            timer: 3000,
                                          });
                                          setIsSubmitting(false);
                                        });
                                    }}
                                    color="danger"
                                    className="denied"
                                  >
                                    Decline
                                  </Button>
                                </th>
                              </>
                            ) : (
                              <th>
                                <Button color="secondary" disabled>
                                  No action
                                </Button>
                              </th>
                            )}
                            <th className="adminth">
                              <div>
                                {moment(booking.createdAt).format(
                                  "D MMM YYYY, k:mm"
                                )}
                              </div>
                            </th>
                          </tr>
                        ))
                      ) : (
                        <tr>
                          <div>
                            <h5>There is no data booking from user </h5>
                          </div>
                        </tr>
                      )
                    ) : (
                      <Loading />
                    )}
                  </Table>
                </TabPane>
              </TabContent>
            </FormGroup>
          </Container>
        </Container>
      </div>
    </div>
  );
};

export default Admin;

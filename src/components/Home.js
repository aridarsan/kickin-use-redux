import {
  Jumbotron,
  Container,
  Button,
  Row,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
} from "reactstrap";
import "../styles/Auth.css";
import "../styles/Home.css";
import header from "../img/header-bg.png";
import { Link, useHistory } from "react-router-dom";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { GET_FIELD } from "../redux/action/actionTypes";
import Loading from "./Loading";
import ModalBookingField from "./ModalBookingField";

const Home = (props) => {
  // const [fields, setFields] = useState([]);
  const dispatch = useDispatch();
  const isLoading = useSelector((state) => state.Field.isLoading);
  const fields = useSelector((state) => state.Field.data);

  const history = useHistory();

  useEffect(() => {
    dispatch({
      type: GET_FIELD,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <div className="text-center">
        <Jumbotron
          style={{
            backgroundImage: `url(${header})`,
            backgroundSize: "cover",
            height: "320px",
          }}
        >
          <Container className="text-center">
            <h2 style={{ color: "#e5e5e5", letterSpacing: "0.2em" }}>
              Let's Play Futsal
            </h2>
            <Link to="/browse-field">
              <Button className="btn-primary-lg">
                <h4
                  style={{
                    marginBottom: "0px",
                    color: "#313131",
                  }}
                >
                  Book Now
                </h4>
              </Button>
            </Link>
          </Container>
        </Jumbotron>
      </div>

      <div>
        <Container>
          <Row style={{ marginRight: "0", marginLeft: "0" }}>
            <h4 className="text-white"> Featured Field</h4>{" "}
            <Link to="/browse-field" className="ml-auto">
              <h4 className="browseField">Browse Field &#10140;</h4>
            </Link>
          </Row>

          <Row className="text-white mt-3">
            {fields.length !== 0 ? (
              fields.slice(1, 5).map((field) => (
                <Col lg={3} md={6} className="class-col">
                  <Card className="card-field">
                    {field.fieldPhotos.slice(0, 1).map((photo) => (
                      <CardImg
                        className="card-img"
                        top
                        width="100%"
                        src={photo.photoUrl}
                        alt="field photo"
                      />
                    ))}

                    <CardBody className="card-body">
                      <CardTitle tag="h4">{field.name}</CardTitle>
                      <CardSubtitle tag="h5" className="mt-0 ml-auto">
                        Rp. {field.price}
                        <i class="fas fa-tag ml-2"></i>
                      </CardSubtitle>
                      <CardText className="mt-2">
                        <i class="fas fa-map-marker-alt"></i>
                        {field.address}
                      </CardText>
                      <Row style={{ padding: "0 16px" }}>
                        <Link className="col-5 view">
                          <Button
                            block
                            className="btn-second"
                            onClick={() => {
                              history.push(`/field-detail/${field.id}`);
                            }}
                          >
                            <i class="fas fa-eye mr-1"></i>
                            View
                          </Button>
                        </Link>

                        <Link className="col-5 view ml-auto">
                          <ModalBookingField
                            name={field.name}
                            id={field.id}
                            isLogin={props.isLogin}
                            setIsLogin={props.setIsLogin}
                            price={field.price}
                          />
                        </Link>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
              ))
            ) : isLoading ? (
              <Loading />
            ) : (
              <div>
                <h4>No Field Data Here</h4>
              </div>
            )}
          </Row>
        </Container>
      </div>
    </>
  );
};

export default Home;

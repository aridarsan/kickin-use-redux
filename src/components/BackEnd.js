import React from "react";
import "../styles/BackEnd.css";
import Backend1 from "../img/backend-1.png";
import Backend2 from "../img/backend-2.jpeg";
// import Backend3 from "../img/backend-3.jpg";
import Backend4 from "../img/backend-4.jpeg";

import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardSubtitle,
} from "reactstrap";

import linkedin from "../icon/linkedin.png";
import gitlab from "../icon/gitlab.png";

function BackEnd() {
  return (
    <>
      <Container className="backEnd">
        <Row>
          <Col sm="4">
            <Card className="card-limit">
              <CardImg top width="100%" src={Backend1} alt="BE-1" />
              <CardBody>
                <CardTitle tag="h5">Novien Ghoziana</CardTitle>
                <CardSubtitle tag="h6" className="mb-2">
                  Back End
                </CardSubtitle>
                <a
                  href="https://www.linkedin.com/in/novien-ghoziana-indanartha/"
                  target="_blank"
                  rel="noreferrer"
                  className="features-os d-inline-flex align-items-center"
                >
                  <img
                    className="icon-store appStore"
                    src={linkedin}
                    alt="linkedin"
                    height="70px"
                    style={{ paddingTop: "12px" }}
                  />
                </a>
                <a
                  href="https://gitlab.com/Novienzi"
                  target="_blank"
                  rel="noreferrer"
                  className="features-os d-inline-flex align-items-center"
                >
                  <img
                    className="icon-store appStore"
                    src={gitlab}
                    alt="gitlab"
                    height="70px"
                    style={{ paddingTop: "12px" }}
                  />
                </a>
              </CardBody>
            </Card>
          </Col>
          <Col sm="4">
            <Card className="card-limit">
              <CardImg top width="100%" src={Backend2} alt="BE-2" />
              <CardBody className="CardBody">
                <CardTitle tag="h5">Moh Fitrah Giffari </CardTitle>
                <CardSubtitle tag="h6" className="mb-2">
                  Back End
                </CardSubtitle>
                <a
                  href="https://www.linkedin.com/in/mohammad-fitrah-giffari-885470142/"
                  target="_blank"
                  rel="noreferrer"
                  className="features-os d-inline-flex align-items-center"
                >
                  <img
                    className="icon-store appStore"
                    src={linkedin}
                    alt="linkedin"
                    height="70px"
                    style={{ paddingTop: "12px" }}
                  />
                </a>
                <a
                  href="https://gitlab.com/fitrahgiffari"
                  target="_blank"
                  rel="noreferrer"
                  className="features-os d-inline-flex align-items-center"
                >
                  <img
                    className="icon-store appStore"
                    src={gitlab}
                    alt="gitlab"
                    height="70px"
                    style={{ paddingTop: "12px" }}
                  />
                </a>
              </CardBody>
            </Card>
          </Col>
          {/* <Col sm ="4">
                    <Card className="card-limit">
                        <CardImg top width="100%" src={Backend3} alt="BE-3" />
                        <CardBody>
                            <CardTitle tag="h5">Muhammad Irfan</CardTitle>
                            <CardSubtitle tag="h6" className="mb-2">Back End</CardSubtitle>
                            <a 
                            href= "https://www.linkedin.com/"
                            target="_blank" 
                            rel="noreferrer"
                            className="features-os d-inline-flex align-items-center"
                            >
                            <img
                                className="icon-store appStore"
                                src={linkedin}
                                alt="linkedin"
                                height="70px"
                                style={{paddingTop:"12px"}}
                            />
                            </a>
                            <a 
                            href= "https://gitlab.com/"
                            target="_blank" 
                            rel="noreferrer"
                            className="features-os d-inline-flex align-items-center"
                            >
                            <img
                                className="icon-store appStore"
                                src={gitlab}
                                alt="gitlab"
                                height="70px"
                                style={{paddingTop:"12px"}}
                            />
                            </a>
                        </CardBody>
                    </Card>
                </Col> */}
          <Col sm="4">
            <Card className="card-limit">
              <CardImg top width="100%" src={Backend4} alt="BE-4" />
              <CardBody>
                <CardTitle tag="h5">Sudi Hardi</CardTitle>
                <CardSubtitle tag="h6" className="mb-2">
                  Back End
                </CardSubtitle>
                <a
                  href="https://www.linkedin.com/in/sudi-hardi-83a431141/"
                  target="_blank"
                  rel="noreferrer"
                  className="features-os d-inline-flex align-items-center"
                >
                  <img
                    className="icon-store appStore"
                    src={linkedin}
                    alt="linkedin"
                    height="70px"
                    style={{ paddingTop: "12px" }}
                  />
                </a>
                <a
                  href="https://gitlab.com/sudihardi"
                  target="_blank"
                  rel="noreferrer"
                  className="features-os d-inline-flex align-items-center"
                >
                  <img
                    className="icon-store appStore"
                    src={gitlab}
                    alt="gitlab"
                    height="70px"
                    style={{ paddingTop: "12px" }}
                  />
                </a>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default BackEnd;

import React from "react";
import { Switch, Route, Link, useRouteMatch } from "react-router-dom";

import { Nav, NavItem, Container, Row, Col } from "reactstrap";

import All from "./All";
import FrontEnd from "./FrontEnd";
import BackEnd from "./BackEnd";
import Mobile from "./Mobile";

import "../styles/About.css";

const About = () => {
  let { path, url } = useRouteMatch();

  return (
    <Container className="About">
      <Row>
        <Col sm="3">
          <Nav vertical>
            <NavItem className="navItem">
              <Link
                className="nav-link"
                to={`${url}/all`}
                activeClassName="active"
              >
                All
              </Link>
            </NavItem>
            <NavItem className="navItem">
              <Link
                className="nav-link"
                to={`${url}/frontend`}
                activeClassName="active"
              >
                Front End
              </Link>
            </NavItem>
            <NavItem className="navItem">
              <Link
                className="nav-link"
                to={`${url}/backend`}
                activeClassName="active"
              >
                Back End
              </Link>
            </NavItem>
            <NavItem className="navItem">
              <Link
                className="nav-link"
                to={`${url}/mobile`}
                activeClassName="active"
              >
                Mobile
              </Link>
            </NavItem>
          </Nav>
        </Col>
        <Col sm="9" className="teamMember">
          <h3>Team member :</h3>
          <Switch>
            <Route exact path={`${path}/mobile`}>
              <Mobile />
            </Route>
            <Route exact path={`${path}/backend`}>
              <BackEnd />
            </Route>
            <Route exact path={`${path}/frontend`}>
              <FrontEnd />
            </Route>
            <Route exact path={`${path}/all`}>
              <All />
            </Route>
          </Switch>
        </Col>
      </Row>
    </Container>
  );
};

export default About;

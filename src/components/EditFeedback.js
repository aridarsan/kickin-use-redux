import React, { useState } from "react";
import {
  Button,
  Col,
  Input,
  Label,
  Modal,
  Row,
  Spinner,
} from "reactstrap";
import ReactStars from "react-rating-stars-component";
import axios from "axios";
import swal from "sweetalert";
import "@fortawesome/fontawesome-free";
import "../styles/Feedback.css";

const EditFeedback = (props) => {
  const [rating, setRating] = useState("");
  const [comment, setComment] = useState("");
  const [isSubmitting, setIsSubmitting] = useState(false);

  const toggleFeedback = () => setModalFeedback(!modalFeedback);
  const [modalFeedback, setModalFeedback] = useState(false);

  const token = localStorage.getItem("token");
  // const baseUrl = "http://kickin.southeastasia.cloudapp.azure.com";
  const baseUrl = "https://binar8-novien-ghoziana.nandaworks.com";

  const ratingChange = (newRating) => {
    setRating(newRating);
    // console.log(rating);
  };

  var config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + token,
    },
  };

  const submitFeedback = (e) => {
    e.preventDefault();
    const data = {
      rating: rating,
      comment: comment,
    };

    axios
      .patch(`${baseUrl}/review/edit?id=${props.id}`, data, config)
      .then((res) => {
        setIsSubmitting(true);
        // console.log(res);
        setModalFeedback(false);
        setIsSubmitting(false);
        swal({
          icon: "success",
          title: "Feedback has been updated",
          text: "Thanks for your feedback",
          type: "success",
          buttons: false,
          timer: 3000,
        });
      })
      .catch((err) => {
        // console.log(err);
        swal({
          icon: "warning",
          title: "Failed to edit feedback",
          text: "Please try again",
          type: "warning",
          buttons: false,
          timer: 3000,
        });
        setIsSubmitting(false);
      });
  };

  return (
    <>
      <Button
        onClick={toggleFeedback}
        color="info"
        block
        className="mt-3 mb-3 direction "
      >
        <i class="fas fa-pen mr-2"></i>
        <strong>Edit Feedback</strong>
      </Button>
      <Modal
        isOpen={modalFeedback}
        toggle={toggleFeedback}
        className="custom-modal-size"
      >
        <br />
        <div className="modal-content">
          <div className="modal-body">
            <Row>
              <Col>
                <Button onClick={toggleFeedback} close aria-label="Cancel">
                  <span aria-hidden>
                    <i class="fas fa-times-circle"></i>
                  </span>
                </Button>
              </Col>
            </Row>
            <h3 className="modal-title" style={{ textAlign: "center" }}>
              Leave your Feedback
            </h3>
            <br/>

            <form onSubmit={submitFeedback}>
              <div className="form-group">
                <Label className="label">Select Rating</Label>
                <ReactStars
                  count={5}
                  defaultValue={props.rating}
                  onChange={ratingChange}
                  size={50}
                  activeColor="#FFCB74"
                />
                ,
              </div>
              <div className="form-group">
                <Label className="label">Your Feedback</Label>
                <Input
                  type="textarea"
                  defaultValue={props.comment}
                  onChange={(e) => setComment(e.target.value)}
                  className="form-control"
                  rows="4"
                />
              </div>
              <div className="form-group">
                {isSubmitting ? (
                  <Button className="col-12 btn-primary" type="submit" disabled>
                    <Spinner color="light" />
                  </Button>
                ) : (
                  <Button
                    type="submit"
                    className="btn btn-primary btn-block btn-lg"
                    value="Submit"
                  >
                    Submit
                  </Button>
                )}
              </div>
            </form>
          </div>
        </div>
      </Modal>
    </>
  );
};

export default EditFeedback;

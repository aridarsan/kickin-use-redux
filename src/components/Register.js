/* eslint-disable no-undef */
/* eslint-disable no-useless-escape */
/* eslint-disable eqeqeq */
import React, { useState } from "react";
import { useHistory, Link } from "react-router-dom";
import axios from "axios";
import swal from "sweetalert";
import "@fortawesome/fontawesome-free";
import "../styles/Auth.css";
import {
  Button,
  Modal,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
  Spinner,
  Alert,
  Col,
  Row,
} from "reactstrap";
// const qs = require("querystring");

const Register = (props) => {
  //Define function
  // const url = "http://kickin.southeastasia.cloudapp.azure.com/auth/register";
  const url = "https://binar8-novien-ghoziana.nandaworks.com/auth/register";
  // const url = "https://kickin-app.herokuapp.com/auth/register";

  const { className } = props;

  const history = useHistory();

  const [fullname, setFullname] = useState(null);
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [confirm, setConfirm] = useState(null);
  const [message, setMessage] = useState(null);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [modal, setModal] = useState(false);
  // const [showLogin, setShowLogin] = useState(false);
  const toggle = () => setModal(!modal);

  const checkerRegister = () => {
    //Password and Email Formatting
    let mailformat = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
    let passwordformat = /^(?=.*[0-9])(?=.*[A-Z]).{8,32}$/g;

    if (!fullname) {
      setMessage("Full Name Must be Filled!");
      return false;
    } else if (!email) {
      setMessage("Email Must be Filled!");
      return false;
    } else if (!email.match(mailformat)) {
      setMessage("Email Invalid !!");
      return false;
    } else if (!password) {
      setMessage("Password Must be Filledd!");
      return false;
    } else if (!password.match(passwordformat)) {
      setMessage("Password Invalid !!");
      return false;
    } else if (!confirm) {
      setMessage("Confirm Password Must be Filled!");
      return false;
    } else if (confirm != password) {
      setMessage("Password Not Match!");
      return false;
    } else {
      setIsSubmitting(true);
    }
  };

  //Submit Action
  const handleSubmit = (e) => {
    // console.log("tersubmit");
    e.preventDefault();

    // return false;
    if (checkerRegister() !== false) {
      const bodyData = {
        fullname: fullname,
        email: email,
        password: password,
      };
      // console.log(bodyData);

      //Post Data
      axios.post(url, bodyData).then((res) => {
        // console.log(res);
        const { id, fullname } = res.data;
        localStorage.setItem("id", id);
        localStorage.setItem("fullname", fullname);
        setIsSubmitting(false);
        setModal(false);
        props.setIsLogin(true);
        history.push("/");
        swal({
          icon: "success",
          title: "Register Succes",
          text: "you can log in now",
          type: "success",
          buttons: false,
          timer: 2000,
        });
      });
    } else {
      // console.log("ada yang salah");
      swal({
        icon: "warning",
        title: "Failed to Register",
        text: "please try again",
        type: "warning",
        buttons: false,
        timer: 2000,
      });
    }
  };

  const handleSubmitGoogle = () => {
          swal({
            icon: "error",
            title: "Failed to Login With Google",
            text: "please try with other way",
            type: "warning",
            buttons: false,
            timer: 2000,
          });
    };

  return (
    <div>
      <Link
        onClick={toggle}
        style={{
          textDecoration: "none",
        }}
      >
        <h5 className=" mb-sm-0 btn-register">Sign Up</h5>
      </Link>

      <Modal
        isOpen={modal}
        toggle={toggle}
        className={className}
        id="modal-register"
      >
        <ModalBody>
          <Row>
            <Col>
              <Button onClick={toggle} close aria-label="Cancel">
                <span aria-hidden>
                  <i class="fas fa-times-circle"></i>
                </span>
              </Button>
            </Col>
          </Row>
          <h3 className="title">Sign Up</h3>
          <FormGroup>
            {message && <Alert color="danger">{message}</Alert>}
          </FormGroup>

          <Form onSubmit={handleSubmit}>
            <FormGroup>
              <Label for="fullname">Full Name</Label>
              <Input
                type="text"
                name="fullname"
                id="fullname"
                autocomplete="username"
                className="col-12 m-auto"
                onChange={(e) => setFullname(e.target.value)}
              />
            </FormGroup>

            <FormGroup>
              <Label for="email">Email</Label>
              <Input
                type="email"
                name="email"
                id="email"
                className="col-12 m-auto"
                onChange={(e) => setEmail(e.target.value)}
              />
            </FormGroup>

            <FormGroup>
              <Label for="password">Password</Label>

              <p>
                (Contain at least one Number, one Uppercase and 8 character
                long)
              </p>

              <Input
                type="password"
                name="password"
                id="password"
                className="col-12 m-auto"
                autocomplete="new-password"
                onChange={(e) => setPassword(e.target.value)}
              />
            </FormGroup>

            <FormGroup>
              <Label for="password">Confirm Password</Label>
              <Input
                type="password"
                name="confirm password"
                id="password"
                className="col-12 m-auto"
                autocomplete="new-password"
                onChange={(e) => setConfirm(e.target.value)}
              />
            </FormGroup>

            <FormGroup>
              {isSubmitting ? (
                <Button className="col-12 btn-primary" type="submit" disabled>
                  <Spinner color="light" />
                </Button>
              ) : (
                <Button className="col-12 btn-primary" type="submit">
                  <i class="fas fa-user-plus mr-2"></i>
                  <strong>Sign Up</strong>
                </Button>
              )}
            </FormGroup>

            <p className="or">Or</p>

            <FormGroup>
              <Button onClick={handleSubmitGoogle} className="col-12 btn-facebook">
                <i class="fab fa-google mr-2"></i>
                Sign Up with Google
              </Button>
            </FormGroup>
          </Form>
        </ModalBody>
      </Modal>
    </div>
  );
};

export default Register;

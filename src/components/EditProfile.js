import React, { useRef, useState } from "react";
import { useHistory } from "react-router-dom";
import { Button, Col, Input, Label, Modal, Row, Spinner } from "reactstrap";
import avatardef from "../icon/avatar2.png";
import axios from "axios";
import Uploady from "@rpldy/uploady";
import swal from "sweetalert";
import "../styles/EditProfile.css";

const EditProfile = (props) => {
  const [modalEditProfile, setModalEditProfile] = useState(false);
  const toggleEditProfile = () => setModalEditProfile(!modalEditProfile);
  const inputFile = useRef(null);
  const onTextClick = () => {
    inputFile.current.click();
  };

  const [email, setEmail] = useState(props.email);
  const [fullname, setFullname] = useState(props.fullname);
  const [bio, setBio] = useState(props.bio);
  const [phone, setPhone] = useState(props.phone);
  const [photo, setPhoto] = useState(props.photo);
  const [rawImage, setRawImage] = useState(null);
  const [isSubmitting, setIsSubmitting] = useState(false);

  //url
  const baseUrl = "https://binar8-novien-ghoziana.nandaworks.com";
  // const baseUrl = "http://13.76.129.177";

  //save data needed
  const token = localStorage.getItem("token");
  const id = localStorage.getItem("id");
  const history = useHistory();

  //authorization
  var config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + token,
    },
  };

  const submitEditProfile = (e) => {
    e.preventDefault();
    const data = {
      fullname: fullname,
      email: email,
      photo: photo,
      bio: bio,
      phoneNumber: phone,
    };

    // console.log(data);

    axios
      .patch(`${baseUrl}/profile?id=${id}`, data, config)
      .then((res) => {
        setIsSubmitting(true);
        // console.log(res);
        setIsSubmitting(false);
        toggleEditProfile(false);
        history.push("/profile");
        swal({
          icon: "success",
          title: "Your Profile has been updated",
          text: "reaload the page to see changed",
          type: "success",
          buttons: false,
          timer: 3000,
        });
      })
      .catch((err) => {
        // console.log(err);
        swal({
          icon: "warning",
          title: "Failed to update profile",
          text: "Please try again",
          type: "warning",
          buttons: false,
          timer: 3000,
        });
      });
    setIsSubmitting(false);
  };

  const upload = (e) => {
    setIsSubmitting(true);
    const formdata = new FormData();
    formdata.append("file", rawImage);
    // console.log(rawImage);
    axios
      .post(`${baseUrl}/files`, formdata, config)
      .then((res) => {
        // console.log("berhasil upload");
        setPhoto(res.data);
        setIsSubmitting(false);
        swal({
          icon: "success",
          title: "Your Photo has been uploaded",
          text: "to change, please aplly",
          type: "success",
          buttons: false,
          timer: 3000,
        });
      })
      .catch((err) => {
        console.log("ini errornya =>", err);
        swal({
          icon: "warning",
          title: "Failed to upload photo",
          text: "Please select picture first and try again",
          type: "warning",
          buttons: false,
          timer: 3000,
        });
      });
    setIsSubmitting(false);
  };

  return (
    <>
      <Button onClick={toggleEditProfile} className="button-profil btn-primary">
        Edit Profil
      </Button>
      <Modal
        isOpen={modalEditProfile}
        toggle={toggleEditProfile}
        className="custom-modal-size"
      >
        <div className="modal-body">
          <Row>
            <Col>
              <Button onClick={toggleEditProfile} close aria-label="Cancel">
                <span aria-hidden>
                  <i class="fas fa-times-circle"></i>
                </span>
              </Button>
            </Col>
          </Row>
          <h3 className="modal-title">Edit Profil</h3>

          <div className="pick-picture ml-auto">
            <img
              src={
                props.photo !== null
                  ? props.photo
                  : photo === undefined
                  ? avatardef
                  : photo
              }
              onClick={onTextClick}
              alt="logo"
              className="pick-picture ml-auto"
              style={{ borderRadius: "50%", cursor: "pointer" }}
            />
          </div>

          <Uploady
            multiple
            grouped
            maxGroupSize={2}
            method="POST"
            destination={{
              url: `${baseUrl}/files`,
              headers: {
                "content-type": "multipart/form-data",
                Authorization: "Bearer " + token,
              },
            }}
          >
            <div className="form-avatar" style={{ textAlign: "center" }}>
              <input
                type="file"
                id="myfile"
                ref={inputFile}
                onChange={(e) => setRawImage(e.target.files[0])}
              />
              <text className="editavatar m-auto" onClick={onTextClick}>
                Choose the picture
              </text>
            </div>
            {/* <form>
              <div className="form-avatar">
                <input
                  ref={inputFile}
                  type="file"
                  id="image"
                  accept="image/png, image/jpeg"
                  onChange={(e) => setRawImage(e.target.files[0])}
                />
                <text className="editavatar" onClick={onTextClick}>
                  Edit Profile Picture
                </text>
              </div>
            </form> */}

            {/* <Input
              type="file"
              id="image"
              accept="image/png, image/jpeg"
              onChange={(e) => setRawImage(e.target.files[0])}
              required
            /> */}
          </Uploady>
          <div className="form-group">
            <Button
              block
              onClick={() => upload()}
              color="link"
              className="btn mt-3 btn-lg upload-btn"
            >
              <i class="fas fa-upload mr-2"></i>
              {isSubmitting ? <Spinner color="light" /> : "Upload Picture"}
            </Button>
          </div>

          <form>
            <div className="form-group">
              <Label className="label">Fullname</Label>
              <Input
                type="text"
                className="input-name"
                defaultValue={props.fullname}
                onChange={(e) => setFullname(e.target.value)}
              />
            </div>

            <div className="form-group">
              <Label className="label">Email</Label>
              <Input
                type="text"
                className="input-name"
                defaultValue={props.email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>

            <div className="form-group">
              <Label className="label">Phone Number</Label>
              <Input
                type="text"
                className="input-name"
                defaultValue={props.phone}
                onChange={(e) => setPhone(e.target.value)}
              />
            </div>

            <div className="form-group">
              <Label className="label">Bio</Label>
              <Input
                type="textarea"
                className="input-bio"
                rows="8"
                cols="50"
                defaultValue={props.bio}
                onChange={(e) => setBio(e.target.value)}
              />
            </div>

            {isSubmitting ? (
              <Button className="col-12 btn-primary" type="submit" disabled>
                <Spinner color="light" />
              </Button>
            ) : (
              <div className="form-group">
                <Button
                  onClick={submitEditProfile}
                  className="btn btn-primary btn-block btn-lg"
                >
                  Apply
                </Button>
              </div>
            )}
          </form>
        </div>
      </Modal>
    </>
  );
};

export default EditProfile;

import {all} from 'redux-saga/effects';
import AuthSaga from './authSaga';
import ProfileSaga from './profileSaga';
import FieldSaga from './fieldSaga';
import ReviewSaga from './reviewSaga';
import BookingSaga from './bookingSaga';
import TicketSaga from './ticketSaga';
import RentalKitSaga from './rentalKitSaga';
import TransactionSaga from './transactionSaga';
import PaymentSaga from './paymentSaga';
import AdminSaga from './adminSaga'

export default function* rootSaga() {
  yield all([
    AuthSaga(),
    ProfileSaga(),
    FieldSaga(),
    ReviewSaga(),
    BookingSaga(),
    TicketSaga(),
    RentalKitSaga(),
    TransactionSaga(),
    PaymentSaga(),
    AdminSaga()
  ]);
}

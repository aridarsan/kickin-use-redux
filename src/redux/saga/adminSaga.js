import {takeLatest, put} from 'redux-saga/effects';
import {
  apiGetPaymentAdmin,
  apiGetBookingAdmin,
  apiPatchPayment,
  apiPatchBooking,
} from '../../common/api/mainApi';
import {getHeaders} from '../../common/function/auth';
import {
  GET_PAYADM,
  GET_PAYADM_SUCCESS,
  GET_PAYADM_FAILED,
  GET_BOOKADM,
  GET_BOOKADM_FAILED,
  GET_BOOKADM_SUCCESS,
  PATCH_PAYMENT,
  PATCH_PAYMENT_FAILED,
  PATCH_PAYMENT_SUCCESS,
  PATCH_BOOKING,
  PATCH_BOOKING_FAILED,
  PATCH_BOOKING_SUCCESS,
} from '../action/actionTypes';

function* GetPaymentAdmin() {
  try {
    const headers = yield getHeaders();

    const resGetPayADM = yield apiGetPaymentAdmin(headers);
    console.log(resGetPayADM.data.data);

    yield put({
      type: GET_PAYADM_SUCCESS,
      payload: resGetPayADM.data.data,
    });
    console.log('Berhasil mengambil data payment admin');
  } catch (e) {
    // ToastAndroid.showWithGravity(
    //   'gagal mengambil data payment admin',
    //   ToastAndroid.SHORT,
    //   ToastAndroid.CENTER,
    // );
    yield put({
      type: GET_PAYADM_FAILED,
    });
  }
}

function* GetBookingAdmin() {
  try {
    const headers = yield getHeaders();

    const resGetBookADM = yield apiGetBookingAdmin(headers);
    console.log(resGetBookADM.data.data);

    yield put({
      type: GET_BOOKADM_SUCCESS,
      payload: resGetBookADM.data.data,
    });
    console.log('Berhasil mengambil data booking admin');
  } catch (e) {
    console.log('databooking>>>', e)
    yield put({
      type: GET_BOOKADM_FAILED,
    });
  }
}

function* PatchPayment(action) {
  try {
    const headers = yield getHeaders();
    console.log(action.payload.id)
    const resPatchPayment = yield apiPatchPayment(action.payload.id, headers, action.payload);
    console.log(resPatchPayment.data);

    yield put({
      type: PATCH_PAYMENT_SUCCESS,
      payload: resPatchPayment.data,
    });
    yield put({
      type: GET_PAYADM,
    });
    yield put({
      type: GET_BOOKADM,
    });
    console.log('Berhasil edit payment admin');
  } catch (e) {
    console.log('gagal edit>>', e)
    yield put({
      type: PATCH_PAYMENT_FAILED,
    });
  }
}

function* PatchBooking(action) {
  try {
    const headers = yield getHeaders();

    const resPatchBooking = yield apiPatchBooking(action.payload.id, headers, action.payload);
    console.log(resPatchBooking.data);

    yield put({
      type: PATCH_BOOKING_SUCCESS,
      payload: resPatchBooking.data,
    });
    yield put({
      type: GET_PAYADM,
    });
    yield put({
      type: GET_BOOKADM,
    });
    console.log('Berhasil edit booking admin');
  } catch (e) {
    // ToastAndroid.showWithGravity(
    //   'gagal edit booking admin',
    //   ToastAndroid.SHORT,
    //   ToastAndroid.CENTER,
    // );
    yield put({
      type: PATCH_BOOKING_FAILED,
    });
  }
}

function* adminSaga() {
  yield takeLatest(GET_PAYADM, GetPaymentAdmin);
  yield takeLatest(GET_BOOKADM, GetBookingAdmin);
  yield takeLatest(PATCH_PAYMENT, PatchPayment);
  yield takeLatest(PATCH_BOOKING, PatchBooking);
}

export default adminSaga;

import {takeLatest, put} from 'redux-saga/effects';
import {
  apiGetBookingFieldList,
  apiBooking,
  apiGetBookingUser,
} from '../../common/api/mainApi';
import {getAccountId, getHeaders} from '../../common/function/auth';
import {
  GET_BOOKING_FIELD,
  GET_BOOKING_FIELD_SUCCESS,
  GET_BOOKING_FIELD_FAILED,
  ADD_BOOKING,
  ADD_BOOKING_SUCCESS,
  ADD_BOOKING_FAILED,
  GET_BOOKING_USER,
  GET_BOOKING_USER_SUCCESS,
  GET_BOOKING_USER_FAILED,
} from '../action/actionTypes';

function* GetBookingFieldListAction(action) {
  try {
    const headers = yield getHeaders();

    const resGetBookingFieldList = yield apiGetBookingFieldList(action.payload, headers);
    console.log('ini data booking', resGetBookingFieldList.data.data);
    yield put({
      type: GET_BOOKING_FIELD_SUCCESS,
      payload: resGetBookingFieldList.data.data,
    });
    console.log('Berhasil mengambil list BOOKING Lapangan');
  } catch (e) {
    // ToastAndroid.showWithGravity(
    //   'gagal mengambil list BOOKING lapangan',
    //   ToastAndroid.SHORT,
    //   ToastAndroid.CENTER,
    // );
    yield put({
      type: GET_BOOKING_FIELD_FAILED,
    });
  }
}

function* BookingAction(action) {
  try {
    const headers = yield getHeaders()

    console.log('ini paylod id',action.payload.id)
    const resBooking = yield apiBooking(
      action.payload.id,
      headers,
      action.payload,)

   
    yield put({
      type: ADD_BOOKING_SUCCESS, 
      payload: resBooking.data.data
    })
  console.log('ini berhasil booking' , resBooking.data)
  } catch (e) {
    console.error('ini gagal booking' , e)
    yield put({type: ADD_BOOKING_FAILED})
  }
}

function* GetBookingUserAction() {
  try {
    const headers = yield getHeaders();
    const accountId = yield getAccountId();

    const resGetUserBooking = yield apiGetBookingUser(accountId, headers);
    console.log('ini data booking USER', resGetUserBooking.data.data)
    yield put({
      type: GET_BOOKING_USER_SUCCESS,
      payload: resGetUserBooking.data.data,
    });
    console.log('Berhasil mengambil data BOOKING USER');
  } catch (e) {
    // ToastAndroid.showWithGravity(
    //   'gagal mengambil data BOOKING USER',
    //   ToastAndroid.SHORT,
    //   ToastAndroid.CENTER,
    // );
    yield put({
      type: GET_BOOKING_USER_FAILED,
    });
  }
}

function* BookingSaga() {
  yield takeLatest(GET_BOOKING_FIELD, GetBookingFieldListAction);
  yield takeLatest(GET_BOOKING_USER, GetBookingUserAction);
  yield takeLatest(ADD_BOOKING, BookingAction);
}

export default BookingSaga;

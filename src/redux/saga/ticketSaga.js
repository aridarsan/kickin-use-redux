import {takeLatest, put} from 'redux-saga/effects';
import {apiAddTicket, apiGetTicket} from '../../common/api/mainApi';
import {getHeaders} from '../../common/function/auth';
import {
  ADD_TICKET,
  ADD_TICKET_SUCCESS,
  ADD_TICKET_FAILED,
  GET_TICKET,
  GET_TICKET_SUCCESS,
  GET_TICKET_FAILED,
  GET_BOOKING_USER,
} from '../action/actionTypes';

function* AddTicketAction(action) {
  try {
    const headers = yield getHeaders();

    const resAddTicket = yield apiAddTicket(action.payload, headers);

    if (resAddTicket && resAddTicket.data) {
      console.log('ahoooy', resAddTicket.data);
    } else {
      console.log('tidak ada');
    }

    console.info('ini resAddTicket', resAddTicket.data);
    yield put({type: ADD_TICKET_SUCCESS, payload: resAddTicket.data});
    yield put({type: GET_BOOKING_USER});
    console.log('berhasil add Ticket');
  } catch (e) {
    // ToastAndroid.showWithGravity(
    //   'Gagal add Ticket',
    //   ToastAndroid.SHORT,
    //   ToastAndroid.CENTER,
    // );
    console.log('gagal add Ticket');
    yield put({type: ADD_TICKET_FAILED});
  }
}

function* GetTicketAction(action) {
  try {
    const headers = yield getHeaders();

    const resGetTicket = yield apiGetTicket(action.payload, headers);

    if (resGetTicket && resGetTicket.data) {
      console.log('ahoooy', resGetTicket.data);
    } else {
      console.log('tidak ada');
    }

    console.info('ini resGetTicket', resGetTicket.data);
    yield put({type: GET_TICKET_SUCCESS, payload: resGetTicket.data});
    console.log('berhasil GET Ticket');
  } catch (e) {
    // ToastAndroid.showWithGravity(
    //   'Gagal GET Ticket',
    //   ToastAndroid.SHORT,
    //   ToastAndroid.CENTER,
    // );
    console.log('gagal get Ticket');
    yield put({type: GET_TICKET_FAILED});
  }
}

function* TicketSaga() {
  yield takeLatest(ADD_TICKET, AddTicketAction);
  yield takeLatest(GET_TICKET, GetTicketAction);
}

export default TicketSaga;

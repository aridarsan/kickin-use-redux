import {takeLatest, put} from 'redux-saga/effects';
import {
 apiPayment
} from '../../common/api/mainApi';
import { getHeaders} from '../../common/function/auth';
import {
  POST_PAYMENT,
  POST_PAYMENT_SUCCESS,
  POST_PAYMENT_FAILED,
} from '../action/actionTypes';

function* PostPaymentAction(action) {
  try { 
    const headers = yield getHeaders();
    console.log('ini headers payload>>>',action.payload.data._parts[0])
    const resPostPayment = yield apiPayment(
      action.payload.id,
      headers,
      action.payload.data,
    );
    console.info('ini resPostPayment', resPostPayment.data);
    yield put({type: POST_PAYMENT_SUCCESS, payload: resPostPayment.data});
    console.log('berhasil resPostPayment');
  } catch (e) {
    console.error('gagal resPostPayment',e);
    yield put({type: POST_PAYMENT_FAILED});
  }
}


function* PaymentSaga() {
  yield takeLatest(POST_PAYMENT, PostPaymentAction);
}

export default PaymentSaga;

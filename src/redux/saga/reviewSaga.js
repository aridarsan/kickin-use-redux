import {takeLatest, put} from 'redux-saga/effects';
import {apiGetReview, apiGetUserReview, apiEditReview} from '../../common/api/mainApi';
import {getAccountId, getHeaders} from '../../common/function/auth';
import {
  GET_REVIEW,
  GET_REVIEW_SUCCESS,
  GET_REVIEW_FAILED,
  GET_USER_REVIEW,
  GET_USER_REVIEW_SUCCESS,
  GET_USER_REVIEW_FAILED,
  EDIT_REVIEW,
  EDIT_REVIEW_SUCCESS,
  EDIT_REVIEW_FAILED,

} from '../action/actionTypes';

function* GetReview(action) {
  try {
    const headers = yield getHeaders();

    const resReview = yield apiGetReview(action.payload, headers);
    console.info('ini resReview', resReview.data);
    yield put({type: GET_REVIEW_SUCCESS, payload: resReview.data});
    console.log('berhasil ambil data review');
  } catch (e) {
    // ToastAndroid.showWithGravity(
    //   'Gagal mengambil data review',
    //   ToastAndroid.SHORT,
    //   ToastAndroid.CENTER,
    // );

    yield put({type: GET_REVIEW_FAILED});
  }
}

function* GetUserReview(action) {
  try {
    const headers = yield getHeaders();
    const accountId = yield getAccountId();

    const resUserReview = yield apiGetUserReview(accountId, headers);
    console.info('ini resUserReview', resUserReview.data);
    yield put({type: GET_USER_REVIEW_SUCCESS, payload: resUserReview.data});
    console.log('berhasil ambil data user review');
  } catch (e) {
      console.log('gagal ambil data user review', e);

    yield put({type: GET_USER_REVIEW_FAILED});
  }
}

function* EditReview(action) {
  try {
    const headers = yield getHeaders();

    const resEditReview = yield apiEditReview(action.payload.id, headers, action.payload);
    console.info('ini resUserReview', resEditReview.data);
    yield put({type: EDIT_REVIEW_SUCCESS, payload: resEditReview.data});
    console.log('berhasil edit review');
    yield put({type: GET_USER_REVIEW});
  } catch (e) {
      console.log('gagal edit review', JSON.stringify(e));

    yield put({type: EDIT_REVIEW_FAILED});
  }
}

function* ReviewSaga() {
  yield takeLatest(GET_REVIEW, GetReview);
  yield takeLatest(GET_USER_REVIEW, GetUserReview);
  yield takeLatest(EDIT_REVIEW, EditReview);
}

export default ReviewSaga;

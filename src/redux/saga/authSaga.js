import { takeLatest, put } from "redux-saga/effects";
import {
  apiLogin,
  apiRegister,
  apiLoginGoogle,
  apiRegisterGoogle,
  apiLoginAdmin,
} from "../../common/api/mainApi";
import swal from "sweetalert";
import {
  removeToken,
  removeAccountId,
  saveAccountId,
  saveToken,
} from "../../common/function/auth";
import {
  LOGIN,
  LOGIN_FAILED,
  LOGIN_SUCCESS,
  LOGIN_ADMIN,
  LOGIN_ADMIN_FAILED,
  LOGIN_ADMIN_SUCCESS,
  LOGIN_GOOGLE,
  LOGIN_GOOGLE_FAILED,
  LOGIN_GOOGLE_SUCCESS,
  LOGOUT,
  LOGOUT_ADMIN,
  REGISTER,
  REGISTER_FAILED,
  REGISTER_SUCCESS,
  REGISTER_GOOGLE,
  REGISTER_GOOGLE_FAILED,
  REGISTER_GOOGLE_SUCCESS,
  GET_PROFILE,
  GET_FIELD,
  GET_BOOKING_USER,
  GET_USER_REVIEW,
  GET_RENT_LIST,
} from "../action/actionTypes";
// import AsyncStorage from '@react-native-async-storage/async-storage';

function* Login(action) {
  try {
    const resLogin = yield apiLogin(action.payload);

    if (resLogin && resLogin.data) {
      const { id, token, role } = resLogin.data;
      localStorage.setItem("id", id);
      localStorage.setItem("token", token);
      localStorage.setItem("role", role);
      swal({
        icon: "success",
        title: "Success Login",
        text: "let's book a field",
        type: "success",
        buttons: false,
        timer: 3000,
      });

      yield put({ type: LOGIN_SUCCESS });
      yield put({ type: GET_PROFILE });
      yield put({ type: GET_FIELD });
      yield put({ type: GET_BOOKING_USER });
      yield put({ type: GET_USER_REVIEW });
      yield put({ type: GET_RENT_LIST });
    } else {
      // show alert
      swal({
        icon: "error",
        title: "Wrong email or password",
        text: "please try again",
        type: "warning",
        buttons: false,
        timer: 2000,
      });
      yield put({ type: LOGIN_FAILED });
    }
  } catch (e) {
    // show alert
    swal({
      icon: "error",
      title: "Wrong email or password",
      text: "please try again",
      type: "warning",
      buttons: false,
      timer: 2000,
    });
    yield put({ type: LOGIN_FAILED });
  }
}

function* LoginAdmin(action) {
  try {
    const resLoginAdmin = yield apiLoginAdmin(action.payload);

    if (resLoginAdmin && resLoginAdmin.data) {
      yield saveToken(resLoginAdmin.data.token);
      yield saveAccountId(resLoginAdmin.data.id);

      yield put({ type: LOGIN_ADMIN_SUCCESS });
      yield put({ type: "GET_PAYADM" });
      yield put({ type: "GET_BOOKADM" });
    } else {
      // show alert
      // ToastAndroid.showWithGravity(
      //   "Login Admin gagal",
      //   ToastAndroid.SHORT,
      //   ToastAndroid.BOTTOM
      // );
      yield put({ type: LOGIN_ADMIN_FAILED });
    }
  } catch (e) {
    console.info("e", e);
    // show alert
    // ToastAndroid.showWithGravity(
    //   "Wrong Password, Please dont be account mafia :D ",
    //   ToastAndroid.SHORT,
    //   ToastAndroid.CENTER
    // );
    yield put({ type: LOGIN_FAILED });
  }
}

function* LoginGoogle(action) {
  try {
    const resLoginGoogle = yield apiLoginGoogle(action.payload);

    if (resLoginGoogle && resLoginGoogle.data) {
      yield saveToken(resLoginGoogle.data.token);
      yield saveAccountId(resLoginGoogle.data.id);

      yield put({ type: LOGIN_GOOGLE_SUCCESS });
      console.log("login google sukses");
      yield put({ type: GET_PROFILE });
      yield put({ type: GET_FIELD });
      yield put({ type: GET_BOOKING_USER });
      yield put({ type: GET_USER_REVIEW });
    } else {
      // show alert
      // ToastAndroid.showWithGravity(
      //   "Login GOOGLE gagal",
      //   ToastAndroid.SHORT,
      //   ToastAndroid.BOTTOM
      // );
      yield put({ type: LOGIN_GOOGLE_FAILED });
    }
  } catch (e) {
    console.info("e", e);
    // show alert
    // ToastAndroid.showWithGravity(
    //   "Wrong Password, Please dont be account mafia :D ",
    //   ToastAndroid.SHORT,
    //   ToastAndroid.CENTER
    // );
    yield put({ type: LOGIN_GOOGLE_FAILED });
  }
}

function* Register(action) {
  try {
    const resRegister = yield apiRegister(action.payload);
    console.log("a");
    if (resRegister && resRegister.data) {
      console.log("b");

      yield put({ type: REGISTER_SUCCESS });

      // ToastAndroid.showWithGravity(
      //   "REGISTER SUKSES LET'S LOGIN",
      //   ToastAndroid.SHORT,
      //   ToastAndroid.CENTER
      // );
      // yield put({type : 'GET_PROFILE'});
    } else {
      // show alert
      // ToastAndroid.showWithGravity(
      //   "Register gagal",
      //   ToastAndroid.SHORT,
      //   ToastAndroid.BOTTOM
      // );
      yield put({ type: REGISTER_FAILED });
    }
  } catch (e) {
    console.info("e", e);
    // show alert
    // ToastAndroid.showWithGravity(
    //   "Gagal REGISTER",
    //   ToastAndroid.SHORT,
    //   ToastAndroid.BOTTOM
    // );
    yield put({ type: REGISTER_FAILED });
  }
}

function* RegisterGoogle(action) {
  try {
    const resRegisterGoogle = yield apiRegisterGoogle(action.payload);

    if (resRegisterGoogle && resRegisterGoogle.data) {
      yield put({ type: REGISTER_GOOGLE_SUCCESS });
      console.log("register google sukses");

      // ToastAndroid.showWithGravity(
      //   "REGISTER SUKSES LET'S LOGIN",
      //   ToastAndroid.SHORT,
      //   ToastAndroid.CENTER
      // );
    } else {
      // show alert
      // ToastAndroid.showWithGravity(
      //   "Register GOOGLE gagal",
      //   ToastAndroid.SHORT,
      //   ToastAndroid.BOTTOM
      // );
      yield put({ type: REGISTER_GOOGLE_FAILED });
    }
  } catch (e) {
    console.info("e", e);
    // show alert
    yield put({ type: REGISTER_GOOGLE_FAILED });
  }
}

function* Logout() {
  try {
    yield removeToken();
    yield removeAccountId();
  } catch (e) {
    console.error("error logout", JSON.stringify(e));
    // ToastAndroid.showWithGravity(
    //   "Gagal logout",
    //   ToastAndroid.SHORT,
    //   ToastAndroid.BOTTOM
    // );
  }
}

function* LogoutAdmin() {
  try {
    yield removeToken();
    yield removeAccountId();
  } catch (e) {
    console.error("error logout admin", JSON.stringify(e));
    // ToastAndroid.showWithGravity(
    //   "Gagal logout admin",
    //   ToastAndroid.SHORT,
    //   ToastAndroid.BOTTOM
    // );
  }
}

function* AuthSaga() {
  yield takeLatest(LOGIN, Login);
  yield takeLatest(LOGIN_ADMIN, LoginAdmin);
  yield takeLatest(LOGIN_GOOGLE, LoginGoogle);
  yield takeLatest(REGISTER, Register);
  yield takeLatest(REGISTER_GOOGLE, RegisterGoogle);
  yield takeLatest(LOGOUT, Logout);
  yield takeLatest(LOGOUT_ADMIN, LogoutAdmin);
}

export default AuthSaga;

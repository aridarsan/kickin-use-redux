import {takeLatest, put} from 'redux-saga/effects';
import {
  apiGetRentalItems,
  apiPostSocks,
  apiPostShoes,
  apiPostVest,
} from '../../common/api/mainApi';
import {getHeaders} from '../../common/function/auth';
import {
  GET_RENT_LIST,
  GET_RENT_LIST_SUCCESS,
  GET_RENT_LIST_FAILED,
  POST_SOCKS,
  POST_SOCKS_SUCCESS,
  POST_SOCKS_FAILED,
  POST_SHOES,
  POST_SHOES_SUCCESS,
  POST_SHOES_FAILED,
  POST_VEST,
  POST_VEST_SUCCESS,
  POST_VEST_FAILED,
} from '../action/actionTypes';

function* GetRentalKitListAction() {
  try {
    const headers = yield getHeaders();
    const resRentList = yield apiGetRentalItems(headers);
    yield put({
      type: GET_RENT_LIST_SUCCESS,
      payload: resRentList.data,
    });
    console.log('Berhasil mengambil data rental kit', resRentList.data);
  } catch (e) {
    console.log('gagal mengambil data rental kit', JSON.stringify(e));
    yield put({
      type: GET_RENT_LIST_FAILED,
    });
  }
}

function* PostItemAction(action) {
  try {
    const headers = yield getHeaders();

    const resPostShoes = yield apiPostShoes(
      action.payload.dataShoes.id,
      headers,
      action.payload.dataShoes,
    );
    const resPostVest = yield apiPostVest(
      action.payload.dataVest.id,
      headers,
      action.payload.dataVest,
    );
    const resPostSocks = yield apiPostSocks(
      action.payload.dataSocks.id,
      headers,
      action.payload.dataSocks,
    );
    console.info('ini resPostSocks', resPostSocks.data);
    yield put({type: POST_SOCKS_SUCCESS, payload: resPostSocks.data});
    
    console.log('berhasil POST_ACTION');
  } catch (e) {
    console.error('gagal POST_ACTION',JSON.stringify(e));
    yield put({type: POST_SOCKS_FAILED});
  }
}

function* PostSocksAction(action) {
  try {
    const headers = yield getHeaders();

    const resPostSocks = yield apiPostSocks(
      action.payload.id,
      headers,
      action.payload,
    );
    console.info('ini resPostSocks', resPostSocks.data);
    yield put({type: POST_SOCKS_SUCCESS, payload: resPostSocks.data});

    console.log('berhasil POST_SOCKS');
  } catch (e) {
    console.error('gagal POST_SOCKS',JSON.stringify(e));
    yield put({type: POST_SOCKS_FAILED});
  }
}

function* PostShoesAction(action) {
  try {
    const headers = yield getHeaders();

    const resPostShoes = yield apiPostShoes(
      action.payload.id,
      headers,
      action.payload,
    );

    console.info('ini resPostShoes', resPostShoes.data);
    yield put({type: POST_SHOES_SUCCESS, payload: resPostShoes.data});
    console.log('berhasil POST_SHOES');
  } catch (e) {
    console.error('gagal POST_SHOES',JSON.stringify(e));
    yield put({type: POST_SHOES_FAILED});
  }
}

function* PostVestAction(action) {
  try {
    const headers = yield getHeaders();

    const resPostVest = yield apiPostVest(
      action.payload.id,
      headers,
      action.payload,
    );

    console.info('ini resPostVest', resPostVest.data);
    yield put({type: POST_VEST_SUCCESS, payload: resPostVest.data});
    console.log('berhasil POST_VEST');
  } catch (e) {
    console.error('gagal POST_VEST',JSON.stringify(e));
    yield put({type: POST_VEST_FAILED});
  }
}
function* RentalKitSaga() {
  yield takeLatest(GET_RENT_LIST, GetRentalKitListAction);
  yield takeLatest(POST_SOCKS, PostSocksAction);
  yield takeLatest(POST_SHOES, PostShoesAction);
  yield takeLatest(POST_VEST, PostVestAction);
  yield takeLatest('POST_ACTION', PostItemAction);

}

export default RentalKitSaga;

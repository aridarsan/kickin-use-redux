import {takeLatest, put} from 'redux-saga/effects';
import {apiGetAllField,apiGetAllFieldDetail} from '../../common/api/mainApi';
import {getHeaders} from '../../common/function/auth';
import {
  GET_FIELD,
  GET_FIELD_SUCCESS,
  GET_FIELD_FAILED,
  GET_FIELD_DETAIL,
  GET_FIELD_DETAIL_FAILED,
  GET_FIELD_DETAIL_SUCCESS,
} from '../action/actionTypes';

function* GetFieldAction() {
  try {
    // const headers = yield getHeaders();
    // const accountId = yield getAccountId();

    const resField = yield apiGetAllField();
    console.log(resField.data)

    yield put({
      type: GET_FIELD_SUCCESS,
      payload: resField.data,
    });
    console.log('Berhasil mengambil data Field');
  } catch (e) {
    yield put({
      type: GET_FIELD_FAILED,
    });
  }
}

function* GetFieldDetailAction(action) {
  try {
    const headers = yield getHeaders();
    console.log('1')
    console.log('action payload', action.payload)
    const resFieldDetail = yield apiGetAllFieldDetail(action.payload , headers);

    console.info('ini resFieldDetail', resFieldDetail.data);
    yield put({type: GET_FIELD_DETAIL_SUCCESS, payload: resFieldDetail.data});
    console.log('berhasil ambil field detail');
  } catch (e) {
    console.log('gagal ambil field detail',e)

    yield put({type: GET_FIELD_DETAIL_FAILED});
  }
} 

function* FieldSaga() {
  yield takeLatest(GET_FIELD, GetFieldAction);
  yield takeLatest(GET_FIELD_DETAIL, GetFieldDetailAction);
}

export default FieldSaga;

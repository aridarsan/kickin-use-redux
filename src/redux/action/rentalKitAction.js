import {GET_RENT_LIST, POST_SOCKS, POST_SHOES, POST_VEST} from './actionTypes';

export const GetRentalListAction = (payload) => {
  return {
    type: GET_RENT_LIST,
    payload,
  };
};

export const PostSocksAction = (payload) => {
  return {
    type: POST_SOCKS,
    payload,
  };
};

export const PostShoesAction = (payload) => {
  return {
    type: POST_SHOES,
    payload,
  };
};

export const PostVestAction = (payload) => {
  return {
    type: POST_VEST,
    payload,
  };
};
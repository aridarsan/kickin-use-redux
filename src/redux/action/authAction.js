import {LOGIN, LOGIN_ADMIN ,LOGOUT, LOGOUT_ADMIN, REGISTER,REGISTER_GOOGLE, LOGIN_GOOGLE} from './actionTypes';

export const LoginAction = (payload) => {
  return {type: LOGIN, payload};
};

export const LoginAdminAction = (payload) => {
  return {type: LOGIN_ADMIN, payload};
};


export const LoginGoogleAction = (payload) => {
  return {type: LOGIN_GOOGLE, payload};
};

export const LogoutAction = () => {
  return {type: LOGOUT};
};

export const LogoutAdminAction = () => {
  return {type: LOGOUT_ADMIN};
};

export const RegisterAction = (payload) => {
  return {type: REGISTER, payload};
};

export const RegisterGoogleAction = (payload) => {
  return {type: REGISTER_GOOGLE, payload};
};


import {GET_BOOKING_FIELD, ADD_BOOKING, GET_BOOKING_USER} from './actionTypes';

export const GetBookingFieldListAction = (payload) => {
  return {
    type: GET_BOOKING_FIELD,
    payload,
  };
};

export const AddBookingAction = (payload) => {
  return {
    type: ADD_BOOKING,
    payload,
  };
};

export const GetBookingUserAction = (payload) => {
  return {
    type: GET_BOOKING_USER,
    payload,
  };
};

import {GET_PROFILE, EDIT_PROFILE} from './actionTypes';

export const GetProfileDetail = () => {
  return {type: GET_PROFILE};
};

export const EditProfileDetail = (payload) => {
  return {
    type: EDIT_PROFILE,
    payload,
  };
};

import {GET_REVIEW, GET_USER_REVIEW, EDIT_REVIEW} from './actionTypes';

export const GetReviewAction = (payload) => {
  return {
    type: GET_REVIEW,
    payload,
  };
};

export const GetUserReviewAction = (payload) => {
  return {
    type: GET_USER_REVIEW,
    payload,
  };
};

export const EditReviewAction = (payload) => {
  return {
    type: EDIT_REVIEW,
    payload,
  };
};

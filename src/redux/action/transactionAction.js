import {POST_TRX, GET_TRX} from './actionTypes';

export const PostTrx = (payload) => {
  return {
    type: POST_TRX,
    payload,
  };
};

export const GetTrx = (payload) => {
  return {
    type: GET_TRX,
    payload,
  };
};
import {GET_FIELD, GET_FIELD_DETAIL} from './actionTypes';

export const GetFieldAction = (payload) => {
  return {
    type: GET_FIELD,
    payload,
  };
};

export const GetFieldDetailAction = (payload) => {
  return {
    type: GET_FIELD_DETAIL,
    payload,
  };
};

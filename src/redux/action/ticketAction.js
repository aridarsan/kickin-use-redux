import {ADD_TICKET, GET_TICKET} from './actionTypes';

export const AddTicketAction = (payload) => {
  return {
    type: ADD_TICKET,
    payload,
  };
};
export const GetTicketAction = (payload) => {
  return {
    type: GET_TICKET,
    payload,
  };
};

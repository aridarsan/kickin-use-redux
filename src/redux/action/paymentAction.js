import {POST_PAYMENT, GET_PAYADM, GET_BOOKADM, PATCH_PAYMENT, PATCH_BOOKING} from './actionTypes';

export const PaymentAction = (payload) => {
  return {type: POST_PAYMENT, payload};
};

export const GetPaymentAdmin = (payload) => {
  return {type: GET_PAYADM, payload};
};

export const GetBookingAdmin = (payload) => {
  return {type: GET_BOOKADM, payload};
};

export const PatchPayment = (payload) => {
  return {type: PATCH_PAYMENT, payload};
};

export const PatchBooking = (payload) => {
  return {type: PATCH_BOOKING, payload};
};


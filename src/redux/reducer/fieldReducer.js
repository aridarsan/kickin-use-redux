import {
  GET_FIELD,
  GET_FIELD_SUCCESS,
  GET_FIELD_FAILED,
} from './reducerTypes';

const intialState = {
  isLoading: false,
  data: [],
};

const Field = (state = intialState, action) => {
  switch (action.type) {
    case GET_FIELD: {
      return {
        ...state,
        isLoading: true,
        // fields:[payload]
      };
    }
    case GET_FIELD_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case GET_FIELD_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    default:
      return state;
  }
};

export default Field;

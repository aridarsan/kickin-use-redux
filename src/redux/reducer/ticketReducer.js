import {
  ADD_TICKET,
  ADD_TICKET_SUCCESS,
  ADD_TICKET_FAILED,
} from './reducerTypes';

const intialState = {
  isLoading: false,
  data: [],
};

const Ticket = (state = intialState, action) => {
  switch (action.type) {
    case ADD_TICKET: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case ADD_TICKET_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case ADD_TICKET_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    default:
      return state;
  }
};

export default Ticket;

import {
  GET_TICKET,
  GET_TICKET_SUCCESS,
  GET_TICKET_FAILED,
} from './reducerTypes';

const intialState = {
  isLoading: false,
  data: [],
};

const GetTicket = (state = intialState, action) => {
  switch (action.type) {
    case GET_TICKET: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_TICKET_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case GET_TICKET_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    default:
      return state;
  }
};

export default GetTicket;

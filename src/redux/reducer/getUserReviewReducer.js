import {
  GET_USER_REVIEW,
  GET_USER_REVIEW_SUCCESS,
  GET_USER_REVIEW_FAILED,
} from './reducerTypes';

const intialState = {
  isLoading: false,
  data: [],
};

const GetUserReview = (state = intialState, action) => {
  switch (action.type) {
    case GET_USER_REVIEW: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_USER_REVIEW_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case GET_USER_REVIEW_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }

    default:
      return state;
  }
};

export default GetUserReview;

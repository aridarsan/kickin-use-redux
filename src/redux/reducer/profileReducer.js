import {
  GET_PROFILE,
  GET_PROFILE_SUCCESS,
  GET_PROFILE_FAILED,
  EDIT_PROFILE,
  EDIT_PROFILE_SUCCESS,
  EDIT_PROFILE_FAILED,
  LOGOUT,
} from './reducerTypes';

// initial state = nilai awal data profile yang ada di store
const initialState = {
  isLoading: false,
  data:[{id: null, fullname: null, email: null, password: null, photo: null}]
};

const Profile = (state = initialState, action) => {
  switch (action.type) {
    case GET_PROFILE: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_PROFILE_SUCCESS: {
      return {
        ...state, isLoading: false,
        data: action.payload,
      };
    }
    case GET_PROFILE_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case EDIT_PROFILE: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case EDIT_PROFILE_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case EDIT_PROFILE_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case LOGOUT: {
      return {
        isLoading: false,
        data: [{
          id: null,
          fullname: null,
          email: null,
          password: null,
          photo: null,
        }]
      };
    }
    default:
      return state;
  }
};

export default Profile;


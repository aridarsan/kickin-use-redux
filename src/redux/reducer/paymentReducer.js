import {POST_PAYMENT, POST_PAYMENT_SUCCESS, POST_PAYMENT_FAILED} from './reducerTypes';

const initialState = {
	isLoading: false,
	data: [],
};

const Payment = (state = initialState, action) => {
	switch (action.type) {
		case POST_PAYMENT: {
			return {
				...state,
				isLoading: true,
			};
		}
		case POST_PAYMENT_SUCCESS: {
			return {
				...state,
				isLoading: false,
				data: action.payload
			};
		}
		case POST_PAYMENT_FAILED: {
			return {
				...state,
				isLoading: false,
			};
		}
		default:
			return state;
	}
};

export default Payment;

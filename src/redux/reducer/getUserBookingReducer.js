import {
  GET_BOOKING_USER,
  GET_BOOKING_USER_SUCCESS,
  GET_BOOKING_USER_FAILED,
} from './reducerTypes';

const intialState = {
  isLoading: false,
  data: [],
};

const GetUserBooking = (state = intialState, action) => {
  switch (action.type) {
    case GET_BOOKING_USER: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_BOOKING_USER_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case GET_BOOKING_USER_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }

    default:
      return state;
  }
};

export default GetUserBooking;

import {
  GET_REVIEW,
  GET_REVIEW_SUCCESS,
  GET_REVIEW_FAILED,
  EDIT_REVIEW,
  EDIT_REVIEW_SUCCESS,
  EDIT_REVIEW_FAILED,
} from './reducerTypes';

const intialState = {
  isLoading: false,
  data: [],
};

const Review = (state = intialState, action) => {
  switch (action.type) {
    case GET_REVIEW: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_REVIEW_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case GET_REVIEW_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case EDIT_REVIEW: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case EDIT_REVIEW_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case EDIT_REVIEW_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    default:
      return state;
  }
};

export default Review;

import {
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  LOGIN_ADMIN,
  LOGIN_ADMIN_SUCCESS,
  LOGIN_ADMIN_FAILED,
  LOGIN_GOOGLE,
  LOGIN_GOOGLE_SUCCESS,
  LOGIN_GOOGLE_FAILED,
  LOGOUT,
  LOGOUT_ADMIN,
  REGISTER,
  REGISTER_SUCCESS,
  REGISTER_FAILED,
  REGISTER_GOOGLE,
  REGISTER_GOOGLE_SUCCESS,
  REGISTER_GOOGLE_FAILED,
} from './reducerTypes';

const initialState = {
  isAdmin:false,
  isLoading: false,
  isLoggedIn: false,
  statusRegister: false,
  data: [],
};

const Auth = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case LOGIN_SUCCESS: {
      return {
        ...state,
        isLoggedIn: true,
        isLoading: false,
      };
    }
    case LOGIN_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case LOGIN_ADMIN: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case LOGIN_ADMIN_SUCCESS: {
      return {
        ...state,
        isAdmin: true,
        isLoading: false,
      };
    }
    case LOGIN_ADMIN_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case LOGIN_GOOGLE: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case LOGIN_GOOGLE_SUCCESS: {
      return {
        ...state,
        isLoggedIn: true,
        isLoading: false,
      };
    }
    case LOGIN_GOOGLE_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case REGISTER: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case REGISTER_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        statusRegister: true,
      };
    }
    case REGISTER_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case REGISTER_GOOGLE: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case REGISTER_GOOGLE_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        statusRegister: true,
      };
    }
    case REGISTER_GOOGLE_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case LOGOUT: {
      return {
        ...state,
        isLoggedIn: false,
        isLoading: false,
      };
    }
    case LOGOUT_ADMIN: {
      return {
        ...state,
        isAdmin: false,
        isLoading: false,
      };
    }
    default:
      return state;
  }
};

export default Auth;

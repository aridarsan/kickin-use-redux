import {
  GET_BOOKADM,
  GET_BOOKADM_SUCCESS,
  GET_BOOKADM_FAILED,
} from './reducerTypes';

const intialState = {
  isLoading: false,
  data: [],
};

const GetBookingAdmin = (state = intialState, action) => {
  switch (action.type) {
    case GET_BOOKADM: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_BOOKADM_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case GET_BOOKADM_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    default:
      return state;
  }
};

export default GetBookingAdmin;

import {
  GET_BOOKING_FIELD,
  GET_BOOKING_FIELD_SUCCESS,
  GET_BOOKING_FIELD_FAILED,
} from './reducerTypes';

const intialState = {
  isLoading: false,
  data: [],
};

const GetBookingFieldList = (state = intialState, action) => {
  switch (action.type) {
    case GET_BOOKING_FIELD: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_BOOKING_FIELD_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case GET_BOOKING_FIELD_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    default:
      return state;
  }
};

export default GetBookingFieldList;

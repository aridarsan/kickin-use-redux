import {
  GET_RENT_LIST,
  GET_RENT_LIST_SUCCESS,
  GET_RENT_LIST_FAILED,
  POST_SOCKS,
  POST_SOCKS_SUCCESS,
  POST_SOCKS_FAILED,
  POST_SHOES,
  POST_SHOES_SUCCESS,
  POST_SHOES_FAILED,
  POST_VEST,
  POST_VEST_SUCCESS,
  POST_VEST_FAILED,
} from './reducerTypes';

const intialState = {
  isLoading: false,
  data: {},
};

const GetRentalKitList = (state = intialState, action) => {
  switch (action.type) {
    case GET_RENT_LIST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_RENT_LIST_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case GET_RENT_LIST_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case POST_SOCKS: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case POST_SOCKS_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case POST_SOCKS_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case POST_SHOES: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case POST_SHOES_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case POST_SHOES_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case POST_VEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case POST_VEST_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case POST_VEST_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    default:
      return state;
  }
};

export default GetRentalKitList;

import {
  GET_FIELD_DETAIL,
  GET_FIELD_DETAIL_SUCCESS,
  GET_FIELD_DETAIL_FAILED,
} from './reducerTypes';

const intialState = {
  isLoading: false,
  data: {fieldPhotos : []},
};

const FieldDetail = (state = intialState, action) => {
  switch (action.type) {
    case GET_FIELD_DETAIL: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_FIELD_DETAIL_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case GET_FIELD_DETAIL_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    default:
      return state;
  }
};

export default FieldDetail;

import {combineReducers} from 'redux';
import Auth from './authReducer';
import Profile from './profileReducer';
import Field from './fieldReducer';
import FieldDetail from './fieldDetailReducer';
import Review from './reviewReducer';
import Booking from './addBookingReducer';
import Ticket from './ticketReducer';
import GetTicket from './getTicketReducer';
import GetUserBooking from './getUserBookingReducer';
import GetUserReview from './getUserReviewReducer';
import GetBookingFieldList from './getBookingFieldListReducer';
import GetRentalKitList from './rentalKitReducer';
import Transaction from './transactionReducer';
import Payment from './paymentReducer';
import GetPaymentAdmin from './getPaymentAdmin'
import GetBookingAdmin from './getBookingAdmin'
import PatchAdmin from './adminReducer'

export default combineReducers({
  Auth,
  Profile,
  Field,
  FieldDetail,
  Review,
  Booking,
  Ticket,
  GetTicket,
  GetUserBooking,
  GetUserReview,
  GetBookingFieldList,
  GetRentalKitList,
  Transaction,
  Payment,
  GetPaymentAdmin,
  GetBookingAdmin,
  PatchAdmin
});

import {
  GET_PAYADM,
  GET_PAYADM_SUCCESS,
  GET_PAYADM_FAILED,
} from './reducerTypes';

const intialState = {
  isLoading: false,
  data: [],
};

const GetPaymentAdmin = (state = intialState, action) => {
  switch (action.type) {
    case GET_PAYADM: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_PAYADM_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case GET_PAYADM_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    default:
      return state;
  }
};

export default GetPaymentAdmin;

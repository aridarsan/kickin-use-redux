import {
	PATCH_PAYMENT,
	PATCH_PAYMENT_SUCCESS,
	PATCH_PAYMENT_FAILED,
	PATCH_BOOKING,
	PATCH_BOOKING_SUCCESS,
	PATCH_BOOKING_FAILED,
} from './reducerTypes';

const initialState = {
	isLoading: false,
	data: [],
};

const PatchAdmin = (state = initialState, action) => {
	switch (action.type) {
		case PATCH_PAYMENT: {
			return {
				...state,
				isLoading: true,
			};
		}
		case PATCH_PAYMENT_SUCCESS: {
			return {
				...state,
				isLoading: false,
				data: action.payload,
			};
		}
		case PATCH_PAYMENT_FAILED: {
			return {
				...state,
				isLoading: false,
			};
		}
		case PATCH_BOOKING: {
			return {
				...state,
				isLoading: true,
			};
		}
		case PATCH_BOOKING_SUCCESS: {
			return {
				...state,
				isLoading: false,
				data: action.payload,
			};
		}
		case PATCH_BOOKING_FAILED: {
			return {
				...state,
				isLoading: false,
			};
		}
		default:
			return state;
	}
};

export default PatchAdmin;

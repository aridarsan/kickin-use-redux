import {
  ADD_BOOKING,
  ADD_BOOKING_SUCCESS,
  ADD_BOOKING_FAILED,
} from './reducerTypes';

const intialState = {
  isLoading: false,
  data: {startTime: null, endTime: null},
};

const Booking = (state = intialState, action) => {
  switch (action.type) {
    case ADD_BOOKING: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case ADD_BOOKING_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case ADD_BOOKING_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    default:
      return state;
  }
};

export default Booking;

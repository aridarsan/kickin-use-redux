import {
  POST_TRX,
  POST_TRX_SUCCESS,
  POST_TRX_FAILED,
  GET_TRX,
  GET_TRX_SUCCESS,
  GET_TRX_FAILED,
} from './reducerTypes';

const intialState = {
  isLoading: false,
  data: [],
};

const Transaction = (state = intialState, action) => {
  switch (action.type) {
    case POST_TRX: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case POST_TRX_SUCCESS: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case POST_TRX_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case GET_TRX: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_TRX_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case GET_TRX_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    default:
      return state;
  }
};

export default Transaction;

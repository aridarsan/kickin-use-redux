import React, { useState } from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./components/Home";
import About from "./components/About";
import AboutUs from "./components/AboutUs";
import Admin from "./components/Admin";
import BrowseField from "./components/BrowseField";
import FieldDetailCopy from "./components/FieldDetailCopy";
import Footer from "./components/Footer";
import NavBar from "./components/NavBar";
import Profile from "./components/Profile";

const App = () => {
  const [isLogin, setIsLogin] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  return (
    <>
      <NavBar isLogin={isLogin} setIsLogin={setIsLogin} />
      <Switch>
        <Route exact path="/">
        <Home />
        </Route>

        <Route exact path="/profile">
          <Profile
            isLogin={isLogin}
            setIsLogin={setIsLogin}
            isLoading={isLoading}
            setIsLoading={setIsLoading}
          />
        </Route>

        <Route exact path="/browse-field">
          <BrowseField isLogin={isLogin} setIsLogin={setIsLogin} />
        </Route>

        <Route exact path="/about-app">
          <AboutUs />
        </Route>

        <Route path="/about">
          <About />
        </Route>

        <Route exact path="/field-detail/:id">
          <FieldDetailCopy isLogin={isLogin} setIsLogin={setIsLogin} />
        </Route>

        <Route exact path="/admin">
          <Admin isLogin={isLogin} setIsLogin={setIsLogin} />
        </Route>
      </Switch>
      <Footer />
    </>
  );
};

export default App;
